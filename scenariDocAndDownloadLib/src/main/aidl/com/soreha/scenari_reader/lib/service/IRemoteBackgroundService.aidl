package com.soreha.scenari_reader.lib.service;

import com.soreha.scenari_reader.lib.service.IRemoteBackgroundServiceCallback;
import com.soreha.scenari_reader.lib.data.Document;

interface IRemoteBackgroundService {
    void addNewDocumentRequest(in Document document);
    void cancelDownloadForDocuments (in List docIds);
    void pauseDownloadForDocuments (in List docIds);
    void resumeDownloadForDocuments (in List docIds);
    void registerCallback(IRemoteBackgroundServiceCallback callback);
    void unregisterCallback(IRemoteBackgroundServiceCallback callback);
}