package com.soreha.scenari_reader.lib.service;

interface IRemoteBackgroundServiceCallback {  
    void dataChanged(in int progress, in String tag);
    void showActionBarButton();
    void hideActionBarButton();
}

