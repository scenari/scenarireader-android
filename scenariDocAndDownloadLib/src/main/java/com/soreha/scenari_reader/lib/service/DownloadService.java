package com.soreha.scenari_reader.lib.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.soreha.scenari_reader.lib.R;
import com.soreha.scenari_reader.lib.R.drawable;
import com.soreha.scenari_reader.lib.R.id;
import com.soreha.scenari_reader.lib.R.layout;
import com.soreha.scenari_reader.lib.R.string;
import com.soreha.scenari_reader.lib.comm.RequestManager;
import com.soreha.scenari_reader.lib.comm.RequestManager.DocumentRequestTask;
import com.soreha.scenari_reader.lib.data.Document;
import com.soreha.scenari_reader.lib.data.Document.Documents;
import com.soreha.scenari_reader.lib.util.MyLogs;
import com.soreha.scenari_reader.lib.util.UnZipper;

public class DownloadService extends Service implements Observer {

	private BackgroundServiceBinder binder;
	private ArrayList<Document> _documentQueue = new ArrayList<Document>();
	private ArrayList<String> _queueDocIds = new ArrayList<String>();
	private final RemoteCallbackList<IRemoteBackgroundServiceCallback> _callbacks = new RemoteCallbackList<IRemoteBackgroundServiceCallback>();
	private File _appDirectory;
	private Random _random = new Random(Calendar.getInstance().getTimeInMillis());
	private DocumentRequestTask _documentRequest = null;
	private RequestManager _requestManager;
	private BroadcastReceiver _networkStatusBroadcastReceiver;
	private ContentResolver _contentResolver;
	private static final int NOTIF_ID_DOWNLOADING = -1;
	private NotificationManager mNotificationManager;
	
	public static final String NOTIFICATION_ACTION = "notification_action";
	public static final int ACTION_OPEN_DOWNLOAD_MANAGER = 0;
	public static final int ACTION_OPEN_DOCUMENT = 1;
	public static final String DOCUMENT = "document";


	public class BackgroundServiceBinder extends IRemoteBackgroundService.Stub {
		private DownloadService _service;

		public BackgroundServiceBinder(DownloadService service) {
			_service = service;
		}

		@Override
		public void registerCallback(IRemoteBackgroundServiceCallback callback) throws RemoteException {
			_service.getCallbacks().register(callback);
		}

		@Override
		public void unregisterCallback(IRemoteBackgroundServiceCallback callback) throws RemoteException {
			_service.getCallbacks().unregister(callback);
		}

		@Override
		public void addNewDocumentRequest(Document document) throws RemoteException {
			addDocumentRequest(document);
		}

		@SuppressWarnings("rawtypes")
		@Override
		public void cancelDownloadForDocuments(List docIds) throws RemoteException {
			int size = docIds.size();
			boolean stopCurrentRequest = false;
			for (int i = 0; i < size; i++) {
				int index = _queueDocIds.indexOf("" + docIds.get(i));
				if (index != -1) {
					Document document = _documentQueue.get(index);
					if (_documentRequest != null && document == _documentRequest._document) {
						stopCurrentRequest = true;
					}

					_documentQueue.remove(document);
					_queueDocIds.remove("" + document._id);
					if (!document._updateAvailable)
						document._status = Document.STATUS_NOT_DOWNLOADED;
					else
						document._status = Document.STATUS_AVAILABLE;
					document._tmpArchivePath = "";
					document.updateDocument(_contentResolver);
				}
			}

			if (stopCurrentRequest && _documentRequest != null)
				_documentRequest.stop();
		}

		@SuppressWarnings("rawtypes")
		@Override
		public void pauseDownloadForDocuments(List docIds) throws RemoteException {
			int size = docIds.size();
			boolean pauseCurrentRequest = false;
			for (int i = 0; i < size; i++) {
				int index = _queueDocIds.indexOf("" + docIds.get(i));
				if (index != -1) {
					Document document = _documentQueue.get(index);
					if (_documentRequest != null && document == _documentRequest._document) {
						pauseCurrentRequest = true;
					}

					document._status = Document.STATUS_DOWNLOAD_PAUSED;
					_documentQueue.remove(document);
					_documentQueue.add(document);
					_queueDocIds.remove("" + document._id);
					_queueDocIds.add(document._id + "");
					document.updateDocumentStatus(_contentResolver);
				}
			}

			if (pauseCurrentRequest && _documentRequest != null) {
				_documentRequest.pause();
			}
		}

		@SuppressWarnings("rawtypes")
		@Override
		public void resumeDownloadForDocuments(List docIds) throws RemoteException {
			int size = docIds.size();
			for (int i = 0; i < size; i++) {
				int index = _queueDocIds.indexOf("" + docIds.get(i));
				if (index != -1) {
					Document document = _documentQueue.get(index);
					MyLogs.d("Document in queue : " + document._title + " - " + document._status);
					if (document._status != Document.STATUS_DOWNLOADING) {
						document._updateAvailable = false;
						document._status = Document.STATUS_WAITING_FOR_DOWNLOAD;
						document.updateDocumentStatus(_contentResolver);
					}
				}
			}
			startNextRequest();
		}
	};

	public RemoteCallbackList<IRemoteBackgroundServiceCallback> getCallbacks() {
		return _callbacks;
	}

	@Override
	public IBinder onBind(Intent intent) {
		binder = new BackgroundServiceBinder(this);
		return binder;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		MyLogs.write("SERVICE CREATED", MyLogs.TITLE);

		_contentResolver = getContentResolver();

		_requestManager = new RequestManager(getApplicationContext(), null);
		_requestManager.addObserver(this);
		try {
			_appDirectory = getApplicationContext().getExternalFilesDir(null);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Enregistrement d'un receiver sur le status de la connexion pour
		// relancer les téléchargement
		_networkStatusBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
					MyLogs.write("NETWORK CONNECTIVITY CHANGED", MyLogs.TITLE);
					ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
					NetworkInfo networkInfo = cm.getActiveNetworkInfo();
							//(NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
					if (networkInfo != null && networkInfo.isConnected()) {
						if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
							MyLogs.write("Network type: " + networkInfo.getTypeName() + " - " + networkInfo.getState() +
									" Network subtype: " + networkInfo.getSubtypeName(), MyLogs.INFO);
						} else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
							MyLogs.write("Network type: " + networkInfo.getTypeName() + " - " + networkInfo.getState() +
									" Network subtype: " + networkInfo.getSubtypeName(), MyLogs.INFO);
						}
						if (_documentQueue != null && _documentQueue.size() != 0)
							startNextRequest();
					}
					else {
						MyLogs.write("Network connection lost", MyLogs.INFO);
					}
				}
			}
		};

		registerReceiver(_networkStatusBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

		// Récupération des téléchargement en cours en cas de crash du service
		Cursor cur = getContentResolver().query(Documents.CONTENT_URI, null, Documents.STATUS + " > 1 OR " + Documents.UPDATE_AVAILABLE + " = 1", null,
				Documents.TITLE);
		while (cur.moveToNext()) {
			Document doc = new Document(cur);
			addDocumentRequest(doc);
		}
		cur.close();
		
		
	}

	public void addDocumentRequest(Document document) {
		boolean isInQueue = false;
		for (Document docInQueue : _documentQueue) {
			if (docInQueue._id == document._id)
				isInQueue = true;
		}

		if (!isInQueue && (document._status != Document.STATUS_AVAILABLE || document._updateAvailable)) {

			if (document._tmpArchivePath.equals(""))
				document._tmpArchivePath = getTmpArchiveName();
			_documentQueue.add(document);
			_queueDocIds.add(document._id + "");

			if (document._status == Document.STATUS_NOT_DOWNLOADED)
				document._status = Document.STATUS_WAITING_FOR_DOWNLOAD;
			document.updateDocumentStatus(_contentResolver);

			MyLogs.d("Document ChannelId :" + document._channelId);
			if (_documentRequest == null)
				startNextRequest();
		}
	}

	public String getTmpArchiveName() {
		String tmpFileName = _appDirectory + "/tmp/" + _random.nextInt(9999999) + ".zip";
		while ((new File(tmpFileName)).exists()) {
			tmpFileName = _appDirectory + "/tmp/" + _random.nextInt(9999999) + ".zip";
		}
		return tmpFileName;
	}

	@Override
	public void onDestroy() {
		this.binder = null;
		_callbacks.kill();
		unregisterReceiver(_networkStatusBroadcastReceiver);
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent, flags, startId);
	}

	public void startNextRequest() {
		MyLogs.d("Nb Documents in queue : " + _documentQueue.size() + " - " + (_documentRequest == null));
		if (_documentRequest == null && _documentQueue.size() > 0) {
			int size = _documentQueue.size();
			int i = 0;
			boolean stop = false;
			while (!stop && i < size) {
				Document document = _documentQueue.get(i);
				MyLogs.write("Document : " + document._title + " - " + document._status + " - " + document._updateAvailable, MyLogs.INFO);
				if (document._status != Document.STATUS_DOWNLOAD_PAUSED && !document._updateAvailable) {
					document._status = Document.STATUS_DOWNLOADING;
					MyLogs.d("Document ChannelId :" + document._channelId);
					document.updateDocumentStatus(_contentResolver);
					_documentRequest = _requestManager.new DocumentRequestTask();
					_documentRequest.execute(document);
					setNotification(true);
					stop = true;
				}
				i++;
			}

			if (_documentRequest == null)
				removeNotification();
		} 
		 else if (_documentQueue.size() == 0 ) {
			 removeNotification();
		 }
		
	}

	public void setNotification(boolean isDownloading) {
		int icon;
		CharSequence tickerText;
		CharSequence contentTitle;
		CharSequence contentText;
		boolean onlyAlertOnce;

		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());

		if (isDownloading) { 
			icon = R.drawable.anim_notif_download;
			tickerText = getString(string.download_serv_notif_ticker_downloading);
			contentTitle = getString(string.download_serv_notif_title_downloading);
			contentText = getString(string.download_serv_notif_text_downloading);
			onlyAlertOnce = true;
		} else {
			icon = R.drawable.ic_stat_doc_downloaded;
			tickerText = getString(string.download_serv_notif_ticker_downloaded);
			contentTitle = getString(string.download_serv_notif_title_downloaded);
			contentText = getString(string.download_serv_notif_text_downloaded);
			onlyAlertOnce = false;
		}

		Bundle bundle = new Bundle();
		bundle.putInt(NOTIFICATION_ACTION, ACTION_OPEN_DOWNLOAD_MANAGER);

		Intent notificationIntent = new Intent();
		notificationIntent.setAction("com.soreha.scenari_reader.MY_ACTION");
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		notificationIntent.putExtras(bundle);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		

		builder.setSmallIcon(icon);
		builder.setTicker(tickerText);
		builder.setContentTitle(contentTitle);
		builder.setContentText(contentText);
		builder.setOnlyAlertOnce(onlyAlertOnce);
		builder.setContentIntent(contentIntent);

		if (mNotificationManager == null)
			mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		mNotificationManager.notify(NOTIF_ID_DOWNLOADING, builder.build());
	}

	public void notifyDocumentDownloaded(Document document) {
		int icon;
		CharSequence tickerText;
		CharSequence contentTitle;
		CharSequence contentText;

		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());

		icon = R.drawable.ic_stat_doc_downloaded;
		tickerText = getString(string.download_serv_notif_ticker_downloaded);
		contentText = getString(string.download_serv_notif_text_downloaded2);
		contentTitle = document._title;

		Bundle bundle = new Bundle();
		bundle.putInt(NOTIFICATION_ACTION, ACTION_OPEN_DOCUMENT);
		bundle.putParcelable(DOCUMENT, document);

		Intent notificationIntent = new Intent();
		notificationIntent.setAction("com.soreha.scenari_reader.MY_ACTION");
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		notificationIntent.putExtras(bundle);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		Bitmap bitmap;
		if (document._smallThumbLocalUri.equals(""))
			bitmap = BitmapFactory.decodeResource(getResources(), drawable.ic_default_thumb);
		else
			bitmap = BitmapFactory.decodeFile(document._smallThumbLocalUri);
		builder.setSmallIcon(icon);
		builder.setLargeIcon(bitmap);
		builder.setTicker(tickerText);
		builder.setContentTitle(contentTitle);
		builder.setContentText(contentText);
		builder.setOnlyAlertOnce(false);
		builder.setContentIntent(contentIntent);
		builder.setAutoCancel(true);
		
		RemoteViews mNotificationView = new RemoteViews("com.soreha.scenari_reader", layout.notification_small);
		mNotificationView.setTextViewText(id.tv_notif_title, contentTitle);
		mNotificationView.setTextViewText(id.tv_notif_subtitle, contentText);
		mNotificationView.setImageViewBitmap(id.iv_notif_thumb, bitmap);
		
		if (document._documentType.equals(Document.DOCUMENT_TYPE_OPALE))
				mNotificationView.setImageViewResource(id.iv_notif_doctype, drawable.ic_type_opale_small);
		else if (document._documentType.equals(Document.DOCUMENT_TYPE_TOPAZE))
				mNotificationView.setImageViewResource(id.iv_notif_doctype, drawable.ic_type_topaze_small);
		
		Notification notification = builder.build();
		
		notification.contentView = mNotificationView;

		if (mNotificationManager == null)
			mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		mNotificationManager.notify(document._id, notification);
	}

	public void removeNotification() {
		if (mNotificationManager == null)
			mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.cancel(NOTIF_ID_DOWNLOADING);
	}

	public void extractFiles(Document document) {
		Random random = new Random(Calendar.getInstance().getTimeInMillis());
		document._tmpFolderPath = _appDirectory + "/tmp/" +
				random.nextInt(9999999);
		document._status = Document.STATUS_EXTRACTING;
		document._statusProgress = 0;
		document.updateDocumentStatus(_contentResolver);

		UnZipper unzipper = new UnZipper(document);
		unzipper.addObserver(this);
		unzipper.unzip();
	}

	@Override
	public void update(Observable observable, Object data) {
		Document document = (Document) data;
		if (observable.equals(_requestManager)) {
			refreshDownloadStatus(document);
		} else {
			refreshExtractStatus(document);
		}
	}

	private void refreshExtractStatus(Document document) {
		if (document._status != Document.STATUS_EXTRACTING) {
			if (new File(document._tmpArchivePath).delete()) {
				MyLogs.write("Archive file deleted", MyLogs.DEBUG);
			} else
				MyLogs.write("Error deleting archive file : " +
						document._tmpArchivePath, MyLogs.ERROR);
			MyLogs.write("Files extracted for document : " + document._title,
					MyLogs.DEBUG);

			if (document._status == Document.STATUS_EXTRACTED) {
				File tmpFile = new File(document._tmpFolderPath);
				File newFile;
				if (document._localUri.equals("")) {
					MyLogs.write("No folder for this document : " + document._title,
							MyLogs.DEBUG);
					document._localUri = document._tmpFolderPath.replace("tmp", "docs");
				}

				newFile = new File(document._localUri);

				if (newFile.exists()) {
					deleteRecursive(newFile);
					MyLogs.write("Document old folder deleted : " + document._title,
							MyLogs.DEBUG);
				}

				tmpFile.renameTo(newFile);
			}

			document._status = Document.STATUS_AVAILABLE;
			document._tmpArchivePath = "";
			document._statusProgress = 0;
			document._updateAvailable = false;
			document.updateDocument(_contentResolver);
			
			notifyDocumentDownloaded(document);
			
			startNextRequest();
		}
		document.updateDocumentProgress(_contentResolver);
	}

	void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles())
				deleteRecursive(child);
		fileOrDirectory.delete();
	}

	private void refreshDownloadStatus(Document document) {
		if (document._status == Document.STATUS_DOWNLOADING) {
			document.updateDocumentProgress(_contentResolver);
		} else if (document._status == Document.STATUS_DOWNLOAD_PAUSED) {
			document.updateDocumentStatus(_contentResolver);
			_documentQueue.remove(document);
			_documentQueue.add(document);
			_queueDocIds.remove("" + document._id);
			_queueDocIds.add("" + document._id);
			_documentRequest = null;
			startNextRequest();
		} else if (document._status == Document.STATUS_DOWNLOAD_INTERRUPTED) {
			_documentRequest = null;
		} else if (document._status == Document.STATUS_NOT_DOWNLOADED || document._status == Document.STATUS_AVAILABLE
				|| document._status == Document.STATUS_DOWNLOAD_ERROR) {
			_documentQueue.remove(document);
			_queueDocIds.remove("" + document._id);
			_documentRequest = null;
			startNextRequest();
		} else {
			_documentQueue.remove(document);
			_queueDocIds.remove("" + document._id);
			_documentRequest = null;
			extractFiles(document);
		}
	}
}
