package com.soreha.scenari_reader.lib.util;

import java.util.Vector;

/**
 * Cette classe est utilisé pour supprimer les caractères spéciaux
 * @author Mory Doukouré - Soreha
 *
 */
public abstract class StringOperation
{

    /** Index du 1er caractere accentué **/
    private static final int MIN = 192;
    /** Index du dernier caractere accentué **/
    private static final int MAX = 255;
    /** Vecteur de correspondance entre accent / sans accent **/
    private static final Vector<String> map = initMap();
    
    /** Initialisation du tableau de correspondance entre les caractères accentués
       * et leur homologues non accentués 
       */
    private static Vector<String> initMap()
    {  Vector<String> Result         = new Vector<String>();
       java.lang.String car  = null;
      
       car = new java.lang.String("A");
       Result.add( car );            /* '\u00C0'   é   alt-0192  */ 
       Result.add( car );            /* '\u00C1'   é   alt-0193  */
       Result.add( car );            /* '\u00C2'   é   alt-0194  */
       Result.add( car );            /* '\u00C3'   é   alt-0195  */
       Result.add( car );            /* '\u00C4'   é   alt-0196  */
       Result.add( car );            /* '\u00C5'   é   alt-0197  */
       car = new java.lang.String("AE");
       Result.add( car );            /* '\u00C6'   é   alt-0198  */
       car = new java.lang.String("C");
       Result.add( car );            /* '\u00C7'   é   alt-0199  */
       car = new java.lang.String("E");
       Result.add( car );            /* '\u00C8'   é   alt-0200  */
       Result.add( car );            /* '\u00C9'   é   alt-0201  */
       Result.add( car );            /* '\u00CA'   é   alt-0202  */
       Result.add( car );            /* '\u00CB'   é   alt-0203  */
       car = new java.lang.String("I");
       Result.add( car );            /* '\u00CC'   é   alt-0204  */
       Result.add( car );            /* '\u00CD'   é   alt-0205  */
       Result.add( car );            /* '\u00CE'   é   alt-0206  */
       Result.add( car );            /* '\u00CF'   é   alt-0207  */
       car = new java.lang.String("D");
       Result.add( car );            /* '\u00D0'   é   alt-0208  */
       car = new java.lang.String("N");
       Result.add( car );            /* '\u00D1'   é   alt-0209  */
       car = new java.lang.String("O");
       Result.add( car );            /* '\u00D2'   é   alt-0210  */
       Result.add( car );            /* '\u00D3'   é   alt-0211  */
       Result.add( car );            /* '\u00D4'   é   alt-0212  */
       Result.add( car );            /* '\u00D5'   é   alt-0213  */
       Result.add( car );            /* '\u00D6'   é   alt-0214  */
       car = new java.lang.String("*");
       Result.add( car );            /* '\u00D7'   é   alt-0215  */
       car = new java.lang.String("0");
       Result.add( car );            /* '\u00D8'   é   alt-0216  */
       car = new java.lang.String("U");
       Result.add( car );            /* '\u00D9'   é   alt-0217  */
       Result.add( car );            /* '\u00DA'   é   alt-0218  */
       Result.add( car );            /* '\u00DB'   é   alt-0219  */
       Result.add( car );            /* '\u00DC'   é   alt-0220  */
       car = new java.lang.String("Y");
       Result.add( car );            /* '\u00DD'   é   alt-0221  */
       car = new java.lang.String("é");
       Result.add( car );            /* '\u00DE'   é   alt-0222  */
       car = new java.lang.String("B");
       Result.add( car );            /* '\u00DF'   é   alt-0223  */
       car = new java.lang.String("a");
       Result.add( car );            /* '\u00E0'   é   alt-0224  */
       Result.add( car );            /* '\u00E1'   é   alt-0225  */
       Result.add( car );            /* '\u00E2'   é   alt-0226  */
       Result.add( car );            /* '\u00E3'   é   alt-0227  */
       Result.add( car );            /* '\u00E4'   é   alt-0228  */
       Result.add( car );            /* '\u00E5'   é   alt-0229  */
       car = new java.lang.String("ae");
       Result.add( car );            /* '\u00E6'   é   alt-0230  */
       car = new java.lang.String("c");
       Result.add( car );            /* '\u00E7'   é   alt-0231  */
       car = new java.lang.String("e");
       Result.add( car );            /* '\u00E8'   é   alt-0232  */
       Result.add( car );            /* '\u00E9'   é   alt-0233  */
       Result.add( car );            /* '\u00EA'   é   alt-0234  */
       Result.add( car );            /* '\u00EB'   é   alt-0235  */
       car = new java.lang.String("i");
       Result.add( car );            /* '\u00EC'   é   alt-0236  */
       Result.add( car );            /* '\u00ED'   é   alt-0237  */
       Result.add( car );            /* '\u00EE'   é   alt-0238  */
       Result.add( car );            /* '\u00EF'   é   alt-0239  */
       car = new java.lang.String("d");
       Result.add( car );            /* '\u00F0'   é   alt-0240  */
       car = new java.lang.String("n");
       Result.add( car );            /* '\u00F1'   é   alt-0241  */
       car = new java.lang.String("o");
       Result.add( car );            /* '\u00F2'   é   alt-0242  */
       Result.add( car );            /* '\u00F3'   é   alt-0243  */
       Result.add( car );            /* '\u00F4'   é   alt-0244  */
       Result.add( car );            /* '\u00F5'   é   alt-0245  */
       Result.add( car );            /* '\u00F6'   é   alt-0246  */
       car = new java.lang.String("/");
       Result.add( car );            /* '\u00F7'   é   alt-0247  */
       car = new java.lang.String("0");
       Result.add( car );            /* '\u00F8'   é   alt-0248  */
       car = new java.lang.String("u");
       Result.add( car );            /* '\u00F9'   é   alt-0249  */
       Result.add( car );            /* '\u00FA'   é   alt-0250  */
       Result.add( car );            /* '\u00FB'   é   alt-0251  */
       Result.add( car );            /* '\u00FC'   é   alt-0252  */
       car = new java.lang.String("y");
       Result.add( car );            /* '\u00FD'   é   alt-0253  */
       car = new java.lang.String("é");
       Result.add( car );            /* '\u00FE'   é   alt-0254  */
       car = new java.lang.String("y");
       Result.add( car );            /* '\u00FF'   é   alt-0255  */
       Result.add( car );            /* '\u00FF'       alt-0255  */
      
       return Result;
    }

    /** Transforme une chaine pouvant contenir des accents dans une version sans accent
     *  @param chaine Chaine a convertir sans accent
     *  @return Chaine dont les accents ont été supprimé
     **/
    public static java.lang.String sansAccent(java.lang.String chaine)
    {  java.lang.StringBuffer Result  = new StringBuffer(chaine);
      
       for(int bcl = 0 ; bcl < Result.length() ; bcl++)
       {   int carVal = chaine.charAt(bcl);
           if( carVal >= MIN && carVal <= MAX )
           {  // Remplacement
              java.lang.String newVal = (java.lang.String)map.get( carVal - MIN );
              Result.replace(bcl, bcl+1,newVal);
           }   
       }
       return Result.toString();
   }
}
