package com.soreha.scenari_reader.lib.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import android.content.Context;

import com.soreha.scenari_reader.lib.util.StringOperation;

public class ChannelRssParser {
	
	
	public static void ParseData(Context context, Channel channel, InputStream inputStream) throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser sp = spf.newSAXParser();

		XMLReader xr = sp.getXMLReader();
		
		ChennelRssHandler catHandler = new ChennelRssHandler(context,channel);
		xr.setContentHandler(catHandler);
		
		xr.parse(new InputSource(new InputStreamReader(inputStream, "UTF-8")));
	}
}


class ChennelRssHandler extends DefaultHandler {
	private StringBuffer buffer;
	private Document tmpDocument;
	private boolean inItem = false, inImage = false;
	private int rssPos = 0;
	private int nbDoc = 0;
	
	private Context _context;
	private Channel _channel;

	public ChennelRssHandler(Context context, Channel channel) {
		_context = context;
		_channel = channel;
	}

	@Override
	public void startDocument() throws SAXException {
	}

	@Override
	public void endDocument() throws SAXException {
		_channel._nbDoc = nbDoc;
		_channel._status = Channel.STATUS_VALIDATED;
		_channel.updateChannel(_context.getContentResolver());
	}


	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		
		if (qName.equals("item")){
			inItem = true;
			tmpDocument = new Document(_context);
			tmpDocument._channelId = _channel._id;
		} else if (qName.equals("image")) {
			inImage = true;
		} else if (qName.equals("mobile:smallThumbnail")) {
			tmpDocument._smallThumbUrl = atts.getValue("url");
		} else if (qName.equals("mobile:largeThumbnail")) {
			tmpDocument._largeThumbUrl = atts.getValue("url");
		}else if (qName.equals("mobile:mobileInf")) {
			tmpDocument._author = atts.getValue("author");
			tmpDocument._version = atts.getValue("version");
			tmpDocument._legalInfos = atts.getValue("legal");
			tmpDocument._keywords = atts.getValue("keywords");
		}else {
			buffer = new StringBuffer();
		}
	}

	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		
		if (qName.equals("title")) {
			if (inItem && !inImage) { 
				tmpDocument._title = buffer.toString();
			} else if (!inImage) {  
				_channel._title = buffer.toString();
			}
		}  else if (qName.equals("description")) {
			if (inItem) {
				tmpDocument._summary = buffer.toString();
				if (tmpDocument._summary.equals(""))
					tmpDocument._summary = "Pas de description";
			} else if (!inImage) { 
				_channel._summary = buffer.toString();
			}
		} else if (qName.equals("pubDate")) {
			if (!inItem) {
				_channel._publicationDate = parseDate(buffer.toString());
			}
		}  else if (qName.equals("mobile:pubDate")) {
			tmpDocument._publicationDate = parseDate(buffer.toString());
		} else if (qName.equals("copyright")) {
			_channel._legalInfos = buffer.toString();
		} else if (qName.equals("mobile:lastBuildDate")) {
			tmpDocument._lastBuildDate = parseDate(buffer.toString());
		} else if (qName.equals("lastBuildDate")) {
			_channel._lastBuildDate = parseDate(buffer.toString());
		} else if (qName.equals("image")) {
			inImage = false;
		} else if (qName.equals("url")) {
			_channel._thumbUrl = buffer.toString();
		} else if (qName.equals("width")) {
		} else if (qName.equals("height")) {
		} else if (qName.equals("category")) {
			if (inItem)
				tmpDocument._category = buffer.toString();
		} else if (qName.equals("mobile:pubType")) {
			tmpDocument._documentType = buffer.toString();
		}else if (qName.equals("mobile:smartphoneLink")) {
			tmpDocument._downloadUrl = buffer.toString();
		} else if (qName.equals("guid")) {
			tmpDocument._guid = buffer.toString();
		} else if (qName.equals("item")) {
			inItem = false;
			
			if (tmpDocument._guid.equals("")) {
				tmpDocument._guid = tmpDocument._downloadUrl;
			}
			
			tmpDocument._rssPos = rssPos;
			tmpDocument._normalizedSearchFields = StringOperation.sansAccent(tmpDocument._author+" "+tmpDocument._category+" "+tmpDocument._title+" "+tmpDocument._keywords+" "+tmpDocument._summary);
			tmpDocument.saveDocument(_context.getContentResolver(),_context);
			rssPos ++;
			nbDoc ++;
		} else if (qName.equals("link") && inItem){
			tmpDocument._onlineUrl = buffer.toString();
		}
		buffer = null;
	}
	
	private long parseDate(String date){
		SimpleDateFormat parser = null;
		
		if (date.length() == 31 || date.length() == 30) {
			parser = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
		} else if (date.length() == 28 || date.length() == 27) {
			parser = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm Z", Locale.ENGLISH);
		} else if (date.length() == 26 || date.length() == 25) {
			parser = new SimpleDateFormat("dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);
		} else if (date.length() == 23 || date.length() == 22) {
			parser = new SimpleDateFormat("dd MMM yyyy HH:mm Z", Locale.ENGLISH);
		}
		
		Date lastUpdateDate;
		try {
			lastUpdateDate = parser.parse(date);
			return lastUpdateDate.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public void characters(char ch[], int start, int length) {
		String lecture = new String(ch, start, length);
		if (buffer != null)
			buffer.append(lecture);
	}
}
