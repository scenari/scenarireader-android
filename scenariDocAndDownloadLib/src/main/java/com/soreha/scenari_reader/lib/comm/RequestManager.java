package com.soreha.scenari_reader.lib.comm;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.TimeoutException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.soreha.scenari_reader.lib.data.Channel;
import com.soreha.scenari_reader.lib.data.ChannelRssParser;
import com.soreha.scenari_reader.lib.data.Document;
import com.soreha.scenari_reader.lib.util.MyLogs;
import com.soreha.scenari_reader.lib.util.MySimpleEntry;

public class RequestManager extends Observable {

	public final static int STATUS_OK = 0;
	public final static int STATUS_HTTP_ADRESSE_ERROR = 1;
	public final static int STATUS_TIMEOUT = 2;
	public final static int STATUS_ERROR_OTHER = 3;

	public final static int RSS_REQUEST = 0;
	public final static int THUMB_REQUEST = 1;

	public final static int ALL_REQUESTS_DONE = -1;

	private ArrayList<RssRequestAsyncTask> _rssRequestQueue = new ArrayList<RequestManager.RssRequestAsyncTask>();
	private ArrayList<ThumbnailRequestAsyncTask> _thumbnailRequestQueue = new ArrayList<RequestManager.ThumbnailRequestAsyncTask>();

	private int _nbRunningRssRequest = 0, _nbRunningThumbnailRequest = 0;
	private final DecimalFormat _decimalFormartter = new DecimalFormat("#");

	private Context _context;
	private Handler _handler;

	public RequestManager(Context context, Handler handler) {
		_context = context;
		_handler = handler;
	}

	public void addRssRequest(Channel channel) {
		MyLogs.write("Add RSS Request in queue", MyLogs.TITLE);
		_rssRequestQueue.add(new RssRequestAsyncTask(channel));
		launchNextRequests(RSS_REQUEST);
	}

	public void addThumbnailRequest(int documentId, boolean isLargeImage) {
		MyLogs.write("Add Thumbnail Request in queue", MyLogs.TITLE);
		Document document = new Document(_context);
		document.initDataFromDocumentWithId(_context.getContentResolver(), documentId);
		_thumbnailRequestQueue.add(new ThumbnailRequestAsyncTask(document, isLargeImage));
		launchNextRequests(THUMB_REQUEST);
	}

	public void addThumbnailRequestForChannel(int channelId) {
		MyLogs.write("Add Thumbnail Request in queue", MyLogs.TITLE);
		Channel channel = new Channel(_context);
		channel.initDataForId(_context.getContentResolver(), channelId);
		_thumbnailRequestQueue.add(new ThumbnailRequestAsyncTask(channel));
		launchNextRequests(THUMB_REQUEST);
	}

	public void launchNextRequests(int requestType) {
		if (requestType == RSS_REQUEST) {
			if (_rssRequestQueue.size() > 0) {
				while (_nbRunningRssRequest <= 2 && _rssRequestQueue.size() > 0) {
					MyLogs.write("Launch RSS Request", MyLogs.TITLE);
					_rssRequestQueue.get(0).execute("");
					_rssRequestQueue.remove(0);
					_nbRunningRssRequest++;
				}
			} else if (_nbRunningRssRequest == 0) {
				Message msg = _handler.obtainMessage(ALL_REQUESTS_DONE);
				if (_handler != null)
					_handler.dispatchMessage(msg);
			}
		} else {
			while (_nbRunningThumbnailRequest <= 2 && _thumbnailRequestQueue.size() > 0) {
				_thumbnailRequestQueue.get(0).execute("");
				_thumbnailRequestQueue.remove(0);
				_nbRunningThumbnailRequest++;
			}
		}
	}

	public Map.Entry<Integer, String> RssRequest(String url) {
		StringBuilder uri = new StringBuilder(url);
		MyLogs.write("Requete : " + uri, MyLogs.DEBUG);

		try {
			String data = getResponse(uri, false);
			if (!data.equals("Fail"))
				return new MySimpleEntry<Integer, String>(STATUS_OK, data);
			else
				return new MySimpleEntry<Integer, String>(STATUS_HTTP_ADRESSE_ERROR, null);
		} catch (ClientProtocolException e) {
			return new MySimpleEntry<Integer, String>(STATUS_HTTP_ADRESSE_ERROR, null);
		} catch (IOException e) {
			return new MySimpleEntry<Integer, String>(STATUS_ERROR_OTHER, null);
		} catch (TimeoutException e) {
			return new MySimpleEntry<Integer, String>(STATUS_TIMEOUT, null);
		} catch (Exception e) {
			return new MySimpleEntry<Integer, String>(STATUS_ERROR_OTHER, null);
		}
	}

	public Map.Entry<Integer, Bitmap> ThumbnailRequest(String url) {

		MyLogs.write("Requete : " + url, MyLogs.DEBUG);

		Bitmap bm = null;
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
		HttpConnectionParams.setSoTimeout(httpParameters, 10000);
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpGet request = new HttpGet(url);
		HttpResponse response;

		int statusCode = STATUS_OK;

		try {
			response = client.execute(request);
			int status = response.getStatusLine().getStatusCode();
			if (status != HttpStatus.SC_OK) {
				ByteArrayOutputStream ostream = new ByteArrayOutputStream();
				response.getEntity().writeTo(ostream);
				statusCode = STATUS_HTTP_ADRESSE_ERROR;
			} else {
				InputStream content = response.getEntity().getContent();
				BufferedInputStream bis = new BufferedInputStream(content, 8 * 1024);
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 10;
				bm = BitmapFactory.decodeStream(bis, null, null);
			}
		} catch (ClientProtocolException e) {
			bm = null;
			statusCode = STATUS_HTTP_ADRESSE_ERROR;
		} catch (IOException e) {
			bm = null;
			statusCode = STATUS_ERROR_OTHER;
			e.printStackTrace();
		} catch (Exception e) {
			bm = null;
			statusCode = STATUS_ERROR_OTHER;
		}

		return new MySimpleEntry<Integer, Bitmap>(statusCode, bm);
	}

	public class DocumentRequestTask extends AsyncTask<Document, Document, Integer> {

		public final int STATUS_RUNNING = 0;
		public final int STATUS_PAUSED = 1;
		public final int STATUS_STOPPED = 2;
		public final int STATUS_INTERRUPTED = 3;
		public final int STATUS_ERROR = 4;

		public int _status = STATUS_RUNNING;
		public int _progress = 0;
		public Document _document;

		@Override
		protected Integer doInBackground(Document... params) {
			_document = params[0];

			try {
				DocumentRequest(_document);
			} catch (SocketException e) {
				// La connexion a été établie mais interrompue
				_status = STATUS_INTERRUPTED;
				cancel(false);
				MyLogs.write("SOCKET", MyLogs.TITLE);
				e.printStackTrace();
			} catch (MalformedURLException e) {
				_status = STATUS_ERROR;
				cancel(false);
				e.printStackTrace();
			} catch (IOException e) {
				// Impossible d'établir une connexion type unknownhost...
				// généralement réseau indisponible
				MyLogs.write("IO DETECTED", MyLogs.TITLE);
				_status = STATUS_INTERRUPTED;
				cancel(false);
				e.printStackTrace();
			} catch (Exception e) {
				_status = STATUS_ERROR;
				cancel(false);
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Document... values) {
			Document document = values[0];
			if (document._statusProgress >= _progress + 5) {
				// MyLogs.write("Download Progress : " + _progress + "%");
				_progress = document._statusProgress;
				setChanged();
				notifyObservers(document);
			}
		}

		public void pause() {
			_status = STATUS_PAUSED;
			cancel(false);
		}

		public void stop() {
			_status = STATUS_STOPPED;
			cancel(false);
		}

		public void interrupt() {
			_status = STATUS_INTERRUPTED;
			cancel(false);
		}

		@Override
		protected void onCancelled() {
			switch (_status) {
			case STATUS_PAUSED:
				_document._status = Document.STATUS_DOWNLOAD_PAUSED;
				MyLogs.write("DOWNLOAD PAUSED : ", MyLogs.TITLE);
				break;
			case STATUS_STOPPED:
				if (!_document._updateAvailable)
					_document._status = Document.STATUS_NOT_DOWNLOADED;
				else
					_document._status = Document.STATUS_AVAILABLE;
				_document._statusProgress = 0;
				MyLogs.write("DOWNLOAD CANCELLED : ", MyLogs.TITLE);
				break;
			case STATUS_INTERRUPTED:
				_document._status = Document.STATUS_DOWNLOAD_INTERRUPTED;
				break;
			case STATUS_ERROR:
				_document._status = Document.STATUS_DOWNLOAD_ERROR;
				_document._statusProgress = 0;
				MyLogs.write("DOWNLOAD ERROR : ", MyLogs.TITLE);
				break;
			}

			setChanged();
			notifyObservers(_document);
		}

		@Override
		protected void onPostExecute(Integer result) {
			_document._status = Document.STATUS_DOWNLOADED;
			setChanged();
			notifyObservers(_document);
		}

		public int DocumentRequest(Document document) throws SocketException, MalformedURLException, IOException {
			URL url = new URL(document._downloadUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			long gap = 0;
			
			MyLogs.t("Start document Download : " + document._id);
			
			File file = new File(document._tmpArchivePath);
			if (file.exists()) {
				MyLogs.write("Reprise", MyLogs.ERROR);
				gap = file.length();
				connection.setRequestProperty("Range", "bytes=" + (file.length()) + "-");
				connection.setRequestProperty("If-Range", _document._headerLastModified);
				MyLogs.write("Last modified from doc : " + _document._headerLastModified, MyLogs.ERROR);
			}

			_document._headerLastModified = connection.getHeaderField("Last-Modified");

			long downloaded = 0 + gap;
			long total = connection.getContentLength() + gap;

			BufferedInputStream in = new BufferedInputStream(connection.getInputStream());
			FileOutputStream fos = (downloaded == 0) ? new FileOutputStream(document._tmpArchivePath) : new FileOutputStream(document._tmpArchivePath, true);
			BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
			byte[] data = new byte[1024];
			int x = 0;

			while ((x = in.read(data, 0, 1024)) >= 0 && _status == 0) {
				bout.write(data, 0, x);
				downloaded += x;

				try {
					document._statusProgress = Integer.parseInt(_decimalFormartter.format(downloaded * 100 / total));
				} catch (NumberFormatException e) {
				}

				publishProgress(document);
			}

			bout.flush();
			bout.close();
			fos.close();
			in.close();
			return 1;
		}
	}

	/**
	 * Execute une requete et retourne la r�ponse sous forme de String ou "Fail"
	 * si erreur HTTP
	 */
	private String getResponse(StringBuilder uri, boolean isIso) throws ClientProtocolException, IOException, TimeoutException {
		HttpGet request = new HttpGet(uri.toString());
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
		HttpConnectionParams.setSoTimeout(httpParameters, 10000);
		HttpClient client = new DefaultHttpClient(httpParameters);
		HttpResponse response = client.execute(request);
		int status = response.getStatusLine().getStatusCode();

		if (status != HttpStatus.SC_OK) {
			ByteArrayOutputStream ostream = new ByteArrayOutputStream();
			response.getEntity().writeTo(ostream);
			MyLogs.write(ostream.toString(), MyLogs.ERROR);
			return "Fail";
		} else {
			InputStream content = response.getEntity().getContent();
			BufferedReader reader;
			if (isIso)
				reader = new BufferedReader(new InputStreamReader(content, "ISO-8859-1"), 8 * 1024);
			else
				reader = new BufferedReader(new InputStreamReader(content, "UTF8"), 8 * 1024);
			String line;
			String stream = "";
			while ((line = reader.readLine()) != null) {
				stream += line;
			}
			content.close();
			return stream;
		}
	}

	private class RssRequestAsyncTask extends AsyncTask<String, Integer, Boolean> {
		private Channel _channel;
		private int _error;

		public RssRequestAsyncTask(Channel channel) {
			_channel = channel;
		}

		@Override
		protected Boolean doInBackground(String... params) {
			Map.Entry<Integer, String> result = RssRequest(_channel._rssUrl);
			if (result.getKey() == STATUS_OK) {
				try {
					ChannelRssParser.ParseData(_context, _channel, new ByteArrayInputStream(result.getValue().getBytes()));
				} catch (Exception e) {
					e.printStackTrace();
					_error = Channel.RSS_ERROR;
					return false;
				}
				MyLogs.write("Requete Rss OK : " + _channel._rssUrl, MyLogs.DEBUG);
			}
			else {
				switch (result.getKey()) {
				case STATUS_HTTP_ADRESSE_ERROR:
					_error = Channel.URL_ERROR;
					break;
				default:
					_error = Channel.NETWORK_ERROR;
					break;
				}
				MyLogs.write("Requete Rss Error : " + _channel._rssUrl, MyLogs.DEBUG);
				return false;
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			MyLogs.d("Rss request result : " + result);
			if (result == false) {
				Message msg = _handler.obtainMessage(_error);
				Bundle bundle = new Bundle();
				bundle.putParcelable("channel", _channel);
				msg.setData(bundle);
				_handler.dispatchMessage(msg);
			} else if (_channel._nbDoc == 0) {
				Message msg = _handler.obtainMessage(Channel.EMPTY_CHANNEL);
				_handler.dispatchMessage(msg);
			}

			_nbRunningRssRequest--;
			launchNextRequests(RSS_REQUEST);
		}
	}

	private class ThumbnailRequestAsyncTask extends AsyncTask<String, Integer, Integer> {
		private Document _document = null;
		private Channel _channel = null;
		private boolean _isLargeImage = false;

		public ThumbnailRequestAsyncTask(Document document, boolean isLargeImage) {
			_document = document;
			_isLargeImage = isLargeImage;
		}

		public ThumbnailRequestAsyncTask(Channel channel) {
			_channel = channel;
		}

		@Override
		protected Integer doInBackground(String... params) {

			String url;

			if (_document != null) {
				if (_isLargeImage)
					url = _document._largeThumbUrl;
				else
					url = _document._smallThumbUrl;
			} else
				url = _channel._thumbUrl;

			Map.Entry<Integer, Bitmap> result = ThumbnailRequest(url);

			if (result.getKey() == STATUS_OK) {
				try {
					if (_document != null) {
						String ext = ".png";
						if (_isLargeImage)
							ext = "_large.png";
						FileOutputStream out = new FileOutputStream(_context.getExternalFilesDir(null).getAbsolutePath() + "/thumb/doc_" + _document._id + ext);
						result.getValue().compress(Bitmap.CompressFormat.PNG, 90, out);
						if (_isLargeImage)
							_document._largeThumbLocalUri = _context.getExternalFilesDir(null).getAbsolutePath() + "/thumb/doc_" + _document._id + ext;
						else
							_document._smallThumbLocalUri = _context.getExternalFilesDir(null).getAbsolutePath() + "/thumb/doc_" + _document._id + ext;
					} else {
						FileOutputStream out = new FileOutputStream(_context.getExternalFilesDir(null).getAbsolutePath() + "/thumb/ch_" + _channel._id + ".png");
						result.getValue().compress(Bitmap.CompressFormat.PNG, 90, out);
						_channel._thumbLocalUri = _context.getExternalFilesDir(null).getAbsolutePath() + "/thumb/ch_" + _channel._id + ".png";
					}
				} catch (Exception e) {
					e.printStackTrace();
					return 0;
				}
				MyLogs.write("Requete Thumbnail OK", MyLogs.DEBUG);
				return 1;
			}
			else {
				MyLogs.write("Requete Thumbnail Error : ", MyLogs.DEBUG);
				return 0;
			}
		}

		@Override
		protected void onPostExecute(Integer result) {
			_nbRunningThumbnailRequest--;
			launchNextRequests(THUMB_REQUEST);
			if (_document != null)
				_document.updateThumb(_context.getContentResolver());
			else
				_channel.updateChannel(_context.getContentResolver());
		}
	}
}
