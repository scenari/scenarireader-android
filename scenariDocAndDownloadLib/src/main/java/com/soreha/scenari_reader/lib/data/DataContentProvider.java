package com.soreha.scenari_reader.lib.data;

import java.util.HashMap;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.soreha.scenari_reader.lib.data.Bookmark.Bookmarks;
import com.soreha.scenari_reader.lib.data.Bookmark.BookmarksInfos;
import com.soreha.scenari_reader.lib.data.Channel.Channels;
import com.soreha.scenari_reader.lib.data.Document.Documents;
import com.soreha.scenari_reader.lib.util.MyLogs;

public class DataContentProvider extends ContentProvider {

	private static final String DATABASE_NAME = "opale.db";
	private static final int DATABASE_VERSION = 1;
	private static final String DOCUMENTS_TABLE_NAME = "documents";
	private static final String CHANNELS_TABLE_NAME = "channels";
	private static final String BOOKMARKS_TABLE_NAME = "bookmarks";
	private static final String BOOKMARKS_INFO = "bookmarksInfos";
	
	public static final String AUTHORITY = "com.soreha.scenari_reader.lib.data.OpaleContentProvider";

	private static final UriMatcher _uriMatcher;

	private static final int CHANNELS = 1;
	private static final int DOCUMENTS = 2;
	private static final int BOOKMARKS = 3;
	private static final int BOOKMARKS_INFOS = 4;

	private static HashMap<String, String> _documentsProjectionMap;
	private static HashMap<String, String> _channelsProjectionMap;
	private static HashMap<String, String> _bookmarksProjectionMap;
	private static HashMap<String, String> _bookmarksInfosProjectionMap;
	static {
		_uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		_uriMatcher.addURI(AUTHORITY, DOCUMENTS_TABLE_NAME, DOCUMENTS);

		_documentsProjectionMap = new HashMap<String, String>();
		_documentsProjectionMap.put(Documents.DOCUMENT_ID, Documents.DOCUMENT_ID);
		_documentsProjectionMap.put(Documents.CHANNEL_ID, Documents.CHANNEL_ID);
		_documentsProjectionMap.put(Documents.TITLE, Documents.TITLE);
		_documentsProjectionMap.put(Documents.CATEGORY, Documents.CATEGORY);
		_documentsProjectionMap.put(Documents.AUTHOR, Documents.AUTHOR);
		_documentsProjectionMap.put(Documents.KEYWORDS, Documents.KEYWORDS);
		_documentsProjectionMap.put(Documents.SUMMARY, Documents.SUMMARY);
		_documentsProjectionMap.put(Documents.LEGAL_INFOS, Documents.LEGAL_INFOS);
		_documentsProjectionMap.put(Documents.VERSION, Documents.VERSION);
		_documentsProjectionMap.put(Documents.GUID, Documents.GUID);
		_documentsProjectionMap.put(Documents.ONLINE_URL, Documents.ONLINE_URL);
		_documentsProjectionMap.put(Documents.DOWNLOAD_URL, Documents.DOWNLOAD_URL);
		_documentsProjectionMap.put(Documents.LOCAL_URI, Documents.LOCAL_URI);
		_documentsProjectionMap.put(Documents.STATUS, Documents.STATUS);
		_documentsProjectionMap.put(Documents.STATUS_PROGRESS, Documents.STATUS_PROGRESS);
		_documentsProjectionMap.put(Documents.IS_FAVORITE, Documents.IS_FAVORITE);
		_documentsProjectionMap.put(Documents.LAST_BUILD_DATE, Documents.LAST_BUILD_DATE);
		_documentsProjectionMap.put(Documents.PUBLICATION_DATE, Documents.PUBLICATION_DATE);
		_documentsProjectionMap.put(Documents.LAST_OPENED_PAGE, Documents.LAST_OPENED_PAGE);
		_documentsProjectionMap.put(Documents.RSS_POS, Documents.RSS_POS);
		_documentsProjectionMap.put(Documents.THUMB_SMALL_LOCAL_URI, Documents.THUMB_SMALL_LOCAL_URI);
		_documentsProjectionMap.put(Documents.THUMB_SMALL_URL, Documents.THUMB_SMALL_URL);
		_documentsProjectionMap.put(Documents.THUMB_LARGE_LOCAL_URI, Documents.THUMB_LARGE_LOCAL_URI);
		_documentsProjectionMap.put(Documents.THUMB_LARGE_URL, Documents.THUMB_LARGE_URL);
		_documentsProjectionMap.put(Documents.UPDATE_AVAILABLE, Documents.UPDATE_AVAILABLE);
		_documentsProjectionMap.put(Documents.TMP_ARCHIVE_PATH, Documents.TMP_ARCHIVE_PATH);
		_documentsProjectionMap.put(Documents.HEADER_LAST_MODIFIED, Documents.HEADER_LAST_MODIFIED);
		_documentsProjectionMap.put(Documents.NORMALIZED_SEARCH_FIELD, Documents.NORMALIZED_SEARCH_FIELD);
		_documentsProjectionMap.put(Documents.TYPE, Documents.TYPE);

		_uriMatcher.addURI(AUTHORITY, CHANNELS_TABLE_NAME, CHANNELS);

		_channelsProjectionMap = new HashMap<String, String>();
		_channelsProjectionMap.put(Channels.CHANNEL_ID, Channels.CHANNEL_ID);
		_channelsProjectionMap.put(Channels.TITLE, Channels.TITLE);
		_channelsProjectionMap.put(Channels.SUMMARY, Channels.SUMMARY);
		_channelsProjectionMap.put(Channels.RSS_URL, Channels.RSS_URL);
		_channelsProjectionMap.put(Channels.LEGAL_INFOS, Channels.LEGAL_INFOS);
		_channelsProjectionMap.put(Channels.USER_TITLE, Channels.USER_TITLE);
		_channelsProjectionMap.put(Channels.THUMB_LOCAL_URI, Channels.THUMB_LOCAL_URI);
		_channelsProjectionMap.put(Channels.THUMB_URL, Channels.THUMB_URL);
		_channelsProjectionMap.put(Channels.LAST_BUILD_DATE, Channels.LAST_BUILD_DATE);
		_channelsProjectionMap.put(Channels.PUBLICATION_DATE, Channels.PUBLICATION_DATE);
		_channelsProjectionMap.put(Channels.STATUS, Channels.STATUS);
		_channelsProjectionMap.put(Channels.UPDATE_AVAILABLE, Channels.UPDATE_AVAILABLE);
		_channelsProjectionMap.put(Channels.NB_DOCS, Channels.NB_DOCS);
		_channelsProjectionMap.put(Channels.TYPE, Channels.TYPE);

		_uriMatcher.addURI(AUTHORITY, BOOKMARKS_TABLE_NAME, BOOKMARKS);

		_bookmarksProjectionMap = new HashMap<String, String>();
		_bookmarksProjectionMap.put(Bookmarks.BOOKMARK_ID, Bookmarks.BOOKMARK_ID);
		_bookmarksProjectionMap.put(Bookmarks.DOCUMENT_ID, Bookmarks.DOCUMENT_ID);
		_bookmarksProjectionMap.put(Bookmarks.PAGE_URI, Bookmarks.PAGE_URI);
		_bookmarksProjectionMap.put(Bookmarks.PAGE_TITLE, Bookmarks.PAGE_TITLE);
		
		_uriMatcher.addURI(AUTHORITY, BOOKMARKS_INFO, BOOKMARKS_INFOS);

		_bookmarksInfosProjectionMap = new HashMap<String, String>();
		_bookmarksInfosProjectionMap.put(BookmarksInfos.BOOKMARK_ID, "bk."+Bookmarks.BOOKMARK_ID);
		_bookmarksInfosProjectionMap.put(BookmarksInfos.DOCUMENT_ID, Bookmarks.DOCUMENT_ID);
		_bookmarksInfosProjectionMap.put(BookmarksInfos.PAGE_URI, Bookmarks.PAGE_URI);
		_bookmarksInfosProjectionMap.put(BookmarksInfos.PAGE_TITLE, Bookmarks.PAGE_TITLE);
		_bookmarksInfosProjectionMap.put(BookmarksInfos.DOCUMENT_TITLE, Documents.TITLE);
		_bookmarksInfosProjectionMap.put(BookmarksInfos.CHANNEL_TITLE, Channels.TITLE);
	}

	private DatabaseHelper dbHelper;

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE " + DOCUMENTS_TABLE_NAME + " (" +
					"" + Documents.DOCUMENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
					"" + Documents.CHANNEL_ID + " INTEGER," +
					"" + Documents.TITLE + " TEXT," +
					"" + Documents.CATEGORY + " TEXT," +
					"" + Documents.AUTHOR + " TEXT," +
					"" + Documents.KEYWORDS + " TEXT," +
					"" + Documents.SUMMARY + " TEXT," +
					"" + Documents.LEGAL_INFOS + " TEXT," +
					"" + Documents.VERSION + " TEXT," +
					"" + Documents.GUID + " TEXT," +
					"" + Documents.ONLINE_URL + " TEXT," +
					"" + Documents.DOWNLOAD_URL + " TEXT," +
					"" + Documents.LOCAL_URI + " TEXT," +
					"" + Documents.STATUS + " INTEGER," +
					"" + Documents.STATUS_PROGRESS + " INTEGER," +
					"" + Documents.IS_FAVORITE + " INTEGER," +
					"" + Documents.LAST_BUILD_DATE + " INTEGER," +
					"" + Documents.PUBLICATION_DATE + " INTEGER," +
					"" + Documents.LAST_OPENED_PAGE + " TEXT," +
					"" + Documents.RSS_POS + " INTEGER," +
					"" + Documents.THUMB_SMALL_LOCAL_URI + " TEXT," +
					"" + Documents.THUMB_SMALL_URL + " TEXT," +
					"" + Documents.THUMB_LARGE_LOCAL_URI + " TEXT," +
					"" + Documents.THUMB_LARGE_URL + " TEXT," +
					"" + Documents.TMP_ARCHIVE_PATH + " TEXT," +
					"" + Documents.HEADER_LAST_MODIFIED + " TEXT," +
					"" + Documents.UPDATE_AVAILABLE + " INTEGER," +
					"" + Documents.NORMALIZED_SEARCH_FIELD + " TEXT," +
					"" + Documents.TYPE + " TEXT" +
					"" + ");");

			db.execSQL("CREATE TABLE " + CHANNELS_TABLE_NAME + " (" +
					"" + Channels.CHANNEL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
					"" + Channels.TITLE + " TEXT," +
					"" + Channels.SUMMARY + " TEXT," +
					"" + Channels.RSS_URL + " TEXT," +
					"" + Channels.LEGAL_INFOS + " TEXT," +
					"" + Channels.USER_TITLE + " TEXT," +
					"" + Channels.THUMB_LOCAL_URI + " TEXT," +
					"" + Channels.THUMB_URL + " TEXT," +
					"" + Channels.LAST_BUILD_DATE + " INTEGER," +
					"" + Channels.PUBLICATION_DATE + " INTEGER," +
					"" + Channels.STATUS + " INTEGER," +
					"" + Channels.UPDATE_AVAILABLE + " INTEGER," +
					"" + Channels.NB_DOCS + " INTEGER," +
					"" + Channels.TYPE + " TEXT" +
					"" + ");");

			db.execSQL("CREATE TABLE " + BOOKMARKS_TABLE_NAME + " (" +
					"" + Bookmarks.BOOKMARK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
					"" + Bookmarks.DOCUMENT_ID + " INTEGER," +
					"" + Bookmarks.PAGE_URI + " TEXT," +
					"" + Bookmarks.PAGE_TITLE + " TEXT" +
					"" + ");");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Log.w(TAG, "Upgrading database from version " + oldVersion +
			// " to " + newVersion + ", which will destroy all old data");
			// db.execSQL("DROP TABLE IF EXISTS " + DOCUMENT_TABLE_NAME);
			// onCreate(db);
		}
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int count;
		switch (_uriMatcher.match(uri)) {
		case DOCUMENTS:
			count = db.delete(DOCUMENTS_TABLE_NAME, selection, selectionArgs);
			break;
		case CHANNELS:
			count = db.delete(CHANNELS_TABLE_NAME, selection, selectionArgs);
			break;
		case BOOKMARKS:
			count = db.delete(BOOKMARKS_TABLE_NAME, selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

	@Override
	public String getType(Uri uri) {
		switch (_uriMatcher.match(uri)) {
		case DOCUMENTS:
			return Documents.CONTENT_TYPE;
		case CHANNELS:
			return Channels.CONTENT_TYPE;
		case BOOKMARKS:
			return Bookmarks.CONTENT_TYPE;
		case BOOKMARKS_INFOS:
			return BookmarksInfos.CONTENT_TYPE;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues initialValues) {
		long rowId;
		
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		;
		ContentValues values;
		if (initialValues != null) {
			values = new ContentValues(initialValues);
		} else {
			values = new ContentValues();
		}

		switch (_uriMatcher.match(uri)) {
		case DOCUMENTS:
			db = dbHelper.getWritableDatabase();
			rowId = db.insert(DOCUMENTS_TABLE_NAME, Documents.TITLE, values);
			if (rowId > 0) {
				Uri documentUri = ContentUris.withAppendedId(Documents.CONTENT_URI, rowId);
				getContext().getContentResolver().notifyChange(documentUri, null);
				return documentUri;
			}
			throw new SQLException("Failed to insert row into " + uri);
		case CHANNELS:
			rowId = db.insert(CHANNELS_TABLE_NAME, Channels.TITLE, values);
			if (rowId > 0) {
				Uri channelUri = ContentUris.withAppendedId(Channels.CONTENT_URI, rowId);
				getContext().getContentResolver().notifyChange(channelUri, null);
				return channelUri;
			}
			throw new SQLException("Failed to insert row into " + uri);
			
		case BOOKMARKS:
			rowId = db.insert(BOOKMARKS_TABLE_NAME, Bookmarks.PAGE_URI, values);
			if (rowId > 0) {
				Uri bookmarkUri = ContentUris.withAppendedId(Bookmarks.CONTENT_URI, rowId);
				getContext().getContentResolver().notifyChange(bookmarkUri, null);
				return bookmarkUri;
			}
			throw new SQLException("Failed to insert row into " + uri);
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
	}

	@Override
	public boolean onCreate() {
		MyLogs.write("Content Provider Created", MyLogs.TITLE);
		dbHelper = new DatabaseHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		switch (_uriMatcher.match(uri)) {
		case DOCUMENTS:
			qb.setTables(DOCUMENTS_TABLE_NAME);
			qb.setProjectionMap(_documentsProjectionMap);
			break;
		case CHANNELS:
			qb.setTables(CHANNELS_TABLE_NAME);
			qb.setProjectionMap(_channelsProjectionMap);
			break;
		case BOOKMARKS:
			qb.setTables(BOOKMARKS_TABLE_NAME);
			qb.setProjectionMap(_bookmarksProjectionMap);
			break;
		case BOOKMARKS_INFOS:
			qb.setTables(BOOKMARKS_TABLE_NAME + " AS bk " 
					+ " INNER JOIN "+DOCUMENTS_TABLE_NAME+" AS doc ON doc."+Documents.DOCUMENT_ID+"=bk."+Bookmarks.DOCUMENT_ID
					+ " INNER JOIN "+CHANNELS_TABLE_NAME+" AS ch ON doc."+Documents.CHANNEL_ID+"=ch."+Channels.CHANNEL_ID);
			qb.setProjectionMap(_bookmarksInfosProjectionMap);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);

		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int count;
		switch (_uriMatcher.match(uri)) {
		case DOCUMENTS:
			count = db.update(DOCUMENTS_TABLE_NAME, values, selection, selectionArgs);
			break;
		case CHANNELS:
			count = db.update(CHANNELS_TABLE_NAME, values, selection, selectionArgs);
			break;
		case BOOKMARKS:
			count = db.update(BOOKMARKS_TABLE_NAME, values, selection, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}

		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}
}
