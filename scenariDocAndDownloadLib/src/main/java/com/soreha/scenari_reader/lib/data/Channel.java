package com.soreha.scenari_reader.lib.data;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.soreha.scenari_reader.lib.R.string;
import com.soreha.scenari_reader.lib.data.Document.Documents;
import com.soreha.scenari_reader.lib.util.MyLogs;

public class Channel implements Parcelable {

	public final static int STATUS_NOT_VALIDATED = 0;
	public final static int STATUS_VALIDATED = 1;

	public final static int NETWORK_ERROR = 0;
	public final static int RSS_ERROR = 1;
	public final static int URL_ERROR = 2;
	public final static int EMPTY_CHANNEL = 3;

	// Database fields
	public int _id;
	public String _title = "";
	public String _summary = "";
	public String _rssUrl = "";
	public String _legalInfos = "";
	public String _userTitle = "";
	public String _thumbLocalUri = "";
	public String _thumbUrl = "";
	public int _nbDoc = 0;
	public long _publicationDate = 0;
	public long _lastBuildDate = 0;
	public int _status = STATUS_NOT_VALIDATED;
	public boolean _updateAvailable = false;
	public String _channelType;

	public Channel(Context context) {
		_title = context.getString(string.channel_default_title);
		_summary = context.getString(string.channel_default_summary);
	}

	public static final class Channels implements BaseColumns {
		private Channels() {
			
		}

		public static final Uri CONTENT_URI = Uri.parse("content://" + DataContentProvider.AUTHORITY + "/channels");

		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.jwei512.channels";

		public static final String CHANNEL_ID = "_id";
		public static final String TITLE = "ch_title";
		public static final String SUMMARY = "summary";
		public static final String RSS_URL = "rss_url";
		public static final String LEGAL_INFOS = "legal";
		public static final String USER_TITLE = "user_title";
		public static final String THUMB_LOCAL_URI = "thumb_local_uri";
		public static final String THUMB_URL = "thumb_url";
		public static final String LAST_BUILD_DATE = "last_build_date";
		public static final String PUBLICATION_DATE = "pub_date";
		public static final String STATUS = "status";
		public static final String UPDATE_AVAILABLE = "update_available";
		public static final String NB_DOCS = "nb_docs";
		public static final String TYPE = "ch_type";
	}

	// adds a new note with a given title and text
	public void insertChannel(ContentResolver contentResolver) {
		Uri uri = contentResolver.insert(Channels.CONTENT_URI, getContentValues());
		_id = Integer.parseInt(uri.getLastPathSegment());
		MyLogs.d("Channels inserted with id : " + uri.getLastPathSegment());
	}

	private ContentValues getContentValues() {
		ContentValues contentValues = new ContentValues();
		contentValues.put(Channels.TITLE, _title);
		contentValues.put(Channels.RSS_URL, _rssUrl);
		contentValues.put(Channels.SUMMARY, _summary);
		contentValues.put(Channels.LEGAL_INFOS, _legalInfos);
		contentValues.put(Channels.USER_TITLE, _userTitle);
		contentValues.put(Channels.THUMB_LOCAL_URI, _thumbLocalUri);
		contentValues.put(Channels.THUMB_URL, _thumbUrl);
		contentValues.put(Channels.LAST_BUILD_DATE, _lastBuildDate);
		contentValues.put(Channels.PUBLICATION_DATE, _publicationDate);
		contentValues.put(Channels.UPDATE_AVAILABLE, _updateAvailable);
		contentValues.put(Channels.NB_DOCS, _nbDoc);
		contentValues.put(Channels.STATUS, _status);
		return contentValues;
	}

	// checks to see if a note with a given title is in our database
	public static int isChannelInDBForURL(ContentResolver contentResolver, String rssUrl) {
		int ret = -1;
		Cursor cursor = contentResolver.query(Channels.CONTENT_URI, null, Channels.RSS_URL + "='" + rssUrl + "'", null, null);
		if (null != cursor && cursor.moveToNext()) {
			ret = cursor.getInt(cursor.getColumnIndex(Channels.CHANNEL_ID));
		}
		cursor.close();
		return ret;
	}

	// get the note text from the title of the note
	public void initDataForId(ContentResolver contentResolver, int id) {
		Cursor cursor = contentResolver.query(Channels.CONTENT_URI, null, Channels.CHANNEL_ID + "='" + id + "'", null, null);
		if (null != cursor && cursor.moveToNext()) {
			_id = cursor.getInt(cursor.getColumnIndex(Channels.CHANNEL_ID));
			_title = cursor.getString(cursor.getColumnIndex(Channels.TITLE));
			_summary = cursor.getString(cursor.getColumnIndex(Channels.SUMMARY));
			_rssUrl = cursor.getString(cursor.getColumnIndex(Channels.RSS_URL));
			_legalInfos = cursor.getString(cursor.getColumnIndex(Channels.LEGAL_INFOS));
			_userTitle = cursor.getString(cursor.getColumnIndex(Channels.USER_TITLE));
			_thumbLocalUri = cursor.getString(cursor.getColumnIndex(Channels.THUMB_LOCAL_URI));
			_thumbUrl = cursor.getString(cursor.getColumnIndex(Channels.THUMB_URL));
			_lastBuildDate = cursor.getLong(cursor.getColumnIndex(Channels.LAST_BUILD_DATE));
			_publicationDate = cursor.getLong(cursor.getColumnIndex(Channels.PUBLICATION_DATE));
			_status = cursor.getInt(cursor.getColumnIndex(Channels.STATUS));
			_updateAvailable = (cursor.getInt(cursor.getColumnIndex(Channels.UPDATE_AVAILABLE)) == 1);
			_nbDoc = cursor.getInt(cursor.getColumnIndex(Channels.NB_DOCS));
			_channelType = cursor.getString(cursor.getColumnIndex(Channels.TYPE));
		}
		cursor.close();
	}

	public void initDataFromCursor(Cursor cursor) {
		_id = cursor.getInt(cursor.getColumnIndex(Channels.CHANNEL_ID));
		_title = cursor.getString(cursor.getColumnIndex(Channels.TITLE));
		_summary = cursor.getString(cursor.getColumnIndex(Channels.SUMMARY));
		_rssUrl = cursor.getString(cursor.getColumnIndex(Channels.RSS_URL));
		_legalInfos = cursor.getString(cursor.getColumnIndex(Channels.LEGAL_INFOS));
		_userTitle = cursor.getString(cursor.getColumnIndex(Channels.USER_TITLE));
		_thumbLocalUri = cursor.getString(cursor.getColumnIndex(Channels.THUMB_LOCAL_URI));
		_thumbUrl = cursor.getString(cursor.getColumnIndex(Channels.THUMB_URL));
		_lastBuildDate = cursor.getLong(cursor.getColumnIndex(Channels.LAST_BUILD_DATE));
		_publicationDate = cursor.getLong(cursor.getColumnIndex(Channels.PUBLICATION_DATE));
		_status = cursor.getInt(cursor.getColumnIndex(Channels.STATUS));
		_updateAvailable = (cursor.getInt(cursor.getColumnIndex(Channels.UPDATE_AVAILABLE)) == 1);
		_nbDoc = cursor.getInt(cursor.getColumnIndex(Channels.NB_DOCS));
		_channelType = cursor.getString(cursor.getColumnIndex(Channels.TYPE));
	}

	public void updateChannel(ContentResolver contentResolver) {
		contentResolver.update(Channels.CONTENT_URI, getContentValues(), Channels.CHANNEL_ID + "='" + _id + "'", null);
		MyLogs.d("Channel updated for id : " + _id);
	}


	// erases all entries in the database
	public void refreshCache(ContentResolver contentResolver) {
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Parcelable.Creator<Channel> CREATOR = new Parcelable.Creator<Channel>() {
		public Channel createFromParcel(Parcel in) {
			return new Channel(in);
		}

		public Channel[] newArray(int size) {
			return new Channel[size];
		}
	};

	private Channel(Parcel in) {
		readFromParcel(in);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(_id);
		dest.writeString(_title);
		dest.writeString(_summary);
		dest.writeString(_rssUrl);
		dest.writeString(_legalInfos);
		dest.writeString(_userTitle);
		dest.writeString(_thumbLocalUri);
		dest.writeString(_thumbUrl);
		dest.writeInt(_nbDoc);
		dest.writeLong(_publicationDate);
		dest.writeInt(_status);
		dest.writeByte((byte) (_updateAvailable ? 1 : 0));
	}

	public void readFromParcel(Parcel in) {
		_id = in.readInt();
		_title = in.readString();
		_summary = in.readString();
		_rssUrl = in.readString();
		_legalInfos = in.readString();
		_userTitle = in.readString();
		_thumbLocalUri = in.readString();
		_thumbUrl = in.readString();
		_nbDoc = in.readInt();
		_publicationDate = in.readLong();
		_status = in.readInt();
		_updateAvailable = in.readByte() == 1;
	}

	public void delete(final ContentResolver contentResolver) {
		contentResolver.delete(Channels.CONTENT_URI, Channels.CHANNEL_ID + "='" + _id + "'", null);
		final Cursor cursor = contentResolver.query(Documents.CONTENT_URI, null, Documents.CHANNEL_ID + "='" + _id + "'", null, null);
		new Thread(new Runnable() {
			@Override
			public void run() {

				while (cursor.moveToNext()) {
					Document document = new Document(cursor);
					document.deleteData(contentResolver);
				}
			}
		}).start();

		MyLogs.write("Channel data deleted", MyLogs.TITLE);
	}
}
