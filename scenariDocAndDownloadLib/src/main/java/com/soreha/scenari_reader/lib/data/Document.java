package com.soreha.scenari_reader.lib.data;

import java.io.File;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.soreha.scenari_reader.lib.R.string;
import com.soreha.scenari_reader.lib.util.MyLogs;

public class Document implements Parcelable{
	
	public final static int STATUS_NOT_DOWNLOADED = 0;
	public final static int STATUS_AVAILABLE = 1;
	public final static int STATUS_DOWNLOADED = 2;
	public final static int STATUS_WAITING_FOR_DOWNLOAD = 3;
	public final static int STATUS_DOWNLOADING = 4;
	public final static int STATUS_DOWNLOAD_ERROR = 5;
	public final static int STATUS_DOWNLOAD_PAUSED = 6;
	public final static int STATUS_DOWNLOAD_INTERRUPTED = 7;
	public final static int STATUS_EXTRACTING = 8;
	public final static int STATUS_EXTRACTED = 9;
	public final static int STATUS_EXTRACT_ERROR = 10;
	
	public final static String DOCUMENT_TYPE_OPALE = "opale";
	public final static String DOCUMENT_TYPE_TOPAZE = "topaze";
	public final static String DOCUMENT_TYPE_OTHER = "autre";
 
	//Database fields
	public int _id;
	public int _channelId;
	public String _title = "";
	public String _category = "";
	public String _author = "";
	public String _keywords = "";
	public String _summary = "Pas de description";
	public String _legalInfos = "";
	public String _version = "";
	public String _guid = "";
	public String _downloadUrl = "";
	public String _localUri = "";
	public int _status = STATUS_NOT_DOWNLOADED;
	public int _statusProgress = 0;
	public boolean _isFavorite = false;
	public long _lastBuildDate = 0;
	public long _publicationDate = 0;
	public String _lastOpenedPage = "";
	public int _rssPos = 0;
	public String _smallThumbLocalUri = "";
	public String _smallThumbUrl = "";
	public String _largeThumbLocalUri = "";
	public String _largeThumbUrl = "";
	public boolean _updateAvailable = false;
	public String _tmpArchivePath = "";
	public String _headerLastModified = "";
	public String _normalizedSearchFields = "";
	public String _onlineUrl = "";
	public String _documentType = DOCUMENT_TYPE_OTHER;
	
	//Extra fields for download and extract managment
	public String _tmpFolderPath = "";
	
	public Document(Context context){
		_title = context.getString(string.doc_default_title);
		_summary = context.getString(string.doc_default_summary);
		
	};
	
	public Document(Cursor cursor){
		initDataWithCursor(cursor);
	}
	
	public static final class Documents implements BaseColumns {
		private Documents() {
		}

		public static final Uri CONTENT_URI = Uri.parse("content://" + DataContentProvider.AUTHORITY + "/documents");

		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.jwei512.documents";

		public static final String DOCUMENT_ID = "_id";
		public static final String CHANNEL_ID = "channel_id";
		public static final String TITLE = "title";
		public static final String CATEGORY = "category";
		public static final String AUTHOR = "author";
		public static final String KEYWORDS = "keywords";
		public static final String SUMMARY = "summary";
		public static final String LEGAL_INFOS = "legal";
		public static final String VERSION = "version";
		public static final String GUID = "guid";
		public static final String DOWNLOAD_URL = "url";
		public static final String LOCAL_URI = "local_uri";
		public static final String STATUS = "status";
		public static final String STATUS_PROGRESS = "status_progress";
		public static final String IS_FAVORITE = "is_fav";
		public static final String LAST_BUILD_DATE = "last_build_date";
		public static final String PUBLICATION_DATE = "pub_date";
		public static final String LAST_OPENED_PAGE = "last_opened_page";
		public static final String RSS_POS = "rss_pos";
		public static final String THUMB_SMALL_LOCAL_URI = "thumb_small_local_uri";
		public static final String THUMB_SMALL_URL = "thumb_small_url";
		public static final String THUMB_LARGE_LOCAL_URI = "thumb_large_local_uri";
		public static final String THUMB_LARGE_URL = "thumb_large_url";
		public static final String UPDATE_AVAILABLE = "update_available";
		public static final String TMP_ARCHIVE_PATH = "tmp_archive_path";
		public static final String HEADER_LAST_MODIFIED = "header_last_modified";
		public static final String NORMALIZED_SEARCH_FIELD = "normalized_search_fields";
		public static final String ONLINE_URL = "online_url";
		public static final String TYPE = "doc_type";
	}

	// adds a new note with a given title and text
	public void saveDocument(ContentResolver contentResolver, Context ctx) {
		int id;
		if ((id = isDocumentInDB(contentResolver, _guid)) != -1){
			Document localDoc = new Document(ctx);
			localDoc.initDataFromDocumentWithId(contentResolver, id);
			if (localDoc._lastBuildDate != _lastBuildDate){
				//Recupération des données du document mis é jour
				localDoc._title = _title;
				localDoc._category = _category;
				localDoc._author = _author;
				localDoc._keywords = _keywords;
				localDoc._summary = _summary;
				localDoc._legalInfos = _legalInfos;
				localDoc._version = _version;
				localDoc._downloadUrl = _downloadUrl;
				localDoc._lastBuildDate = _lastBuildDate;
				localDoc._publicationDate = _publicationDate;
				localDoc._rssPos = _rssPos;
				localDoc._normalizedSearchFields = _normalizedSearchFields;
				localDoc._onlineUrl = _onlineUrl;
				localDoc._documentType = _documentType;
				
				//Vérification des url des miniatures et mis é jour au besoin
				if (!localDoc._smallThumbUrl.equals(_smallThumbUrl)){
					MyLogs.d("Thumb deleted");
					deleteRecursive(new File(localDoc._smallThumbLocalUri));
					localDoc._smallThumbLocalUri = "";
					localDoc._smallThumbUrl = _smallThumbUrl;
				} /*else {
					MyLogs.write("Ols thumb uri : "+localDoc._smallThumbLocalUri);
					_smallThumbLocalUri = localDoc._smallThumbLocalUri;
				}*/
				
				if (!localDoc._largeThumbUrl.equals(_largeThumbUrl)){
					deleteRecursive(new File(localDoc._largeThumbLocalUri));
					localDoc._largeThumbLocalUri = "";
					localDoc._largeThumbUrl=_largeThumbUrl;
				} /*else {
					_largeThumbLocalUri = localDoc._largeThumbLocalUri;
				}*/
				
				if (localDoc._status != STATUS_NOT_DOWNLOADED) {
					MyLogs.write("Mis é jour disponible",MyLogs.INFO);
					localDoc._updateAvailable = true;
				} 
				
				localDoc.updateDocument(contentResolver);
			} 
		} else {
			Uri uri = contentResolver.insert(Documents.CONTENT_URI, getContentValues());
			_id = Integer.parseInt(uri.getLastPathSegment());
			MyLogs.d("Document inserted with id : "+uri.getLastPathSegment()+" - Titre : "+_title);
		}
	}

	private ContentValues getContentValues() {
		//MyLogs.write("CHANNEL ID : "+_channelId,MyLogs.TITLE);
		ContentValues contentValues = new ContentValues();
		contentValues.put(Documents.CHANNEL_ID, _channelId);
		contentValues.put(Documents.TITLE, _title);
		contentValues.put(Documents.CATEGORY, _category);
		contentValues.put(Documents.AUTHOR, _author);
		contentValues.put(Documents.KEYWORDS, _keywords);
		contentValues.put(Documents.SUMMARY, _summary);
		contentValues.put(Documents.LEGAL_INFOS, _legalInfos);
		contentValues.put(Documents.VERSION, _version);
		contentValues.put(Documents.GUID, _guid);
		contentValues.put(Documents.DOWNLOAD_URL, _downloadUrl);
		contentValues.put(Documents.LOCAL_URI, _localUri);
		contentValues.put(Documents.STATUS, _status);
		contentValues.put(Documents.STATUS_PROGRESS, _statusProgress);
		contentValues.put(Documents.IS_FAVORITE, _isFavorite);
		contentValues.put(Documents.LAST_BUILD_DATE, _lastBuildDate);
		contentValues.put(Documents.PUBLICATION_DATE, _publicationDate);
		contentValues.put(Documents.LAST_OPENED_PAGE, _lastOpenedPage);
		contentValues.put(Documents.RSS_POS, _rssPos);
		contentValues.put(Documents.THUMB_SMALL_LOCAL_URI, _smallThumbLocalUri);
		contentValues.put(Documents.THUMB_SMALL_URL, _smallThumbUrl);
		contentValues.put(Documents.THUMB_LARGE_LOCAL_URI, _largeThumbLocalUri);
		contentValues.put(Documents.THUMB_LARGE_URL, _largeThumbUrl);
		contentValues.put(Documents.UPDATE_AVAILABLE, _updateAvailable);
		contentValues.put(Documents.TMP_ARCHIVE_PATH, _tmpArchivePath);
		contentValues.put(Documents.HEADER_LAST_MODIFIED, _headerLastModified);
		contentValues.put(Documents.NORMALIZED_SEARCH_FIELD, _normalizedSearchFields);
		contentValues.put(Documents.ONLINE_URL, _onlineUrl);
		contentValues.put(Documents.TYPE, _documentType);
		return contentValues;
	}
	
	private ContentValues getContentValuesForStatusUpdate() {
		ContentValues contentValues = new ContentValues();
		contentValues.put(Documents.STATUS, _status);
		contentValues.put(Documents.HEADER_LAST_MODIFIED, _headerLastModified);
		contentValues.put(Documents.STATUS_PROGRESS, _statusProgress);
		contentValues.put(Documents.CHANNEL_ID, _channelId);
		return contentValues;
	}
	
	private ContentValues getContentValuesForProgressUpdate() {
		ContentValues contentValues = new ContentValues();
		contentValues.put(Documents.STATUS_PROGRESS, _statusProgress);
		return contentValues; 
	}
	
	private ContentValues getContentValuesForThumbUpdate() {
		ContentValues contentValues = new ContentValues();
		contentValues.put(Documents.THUMB_SMALL_LOCAL_URI, _smallThumbLocalUri);
		contentValues.put(Documents.THUMB_SMALL_URL, _smallThumbUrl);
		contentValues.put(Documents.THUMB_LARGE_LOCAL_URI, _largeThumbLocalUri);
		contentValues.put(Documents.THUMB_LARGE_URL, _largeThumbUrl);
		return contentValues; 
	}
	

	// checks to see if a note with a given title is in our database
	public static int isDocumentInDB(ContentResolver contentResolver, String guid) {
		int ret = -1;
		Cursor cursor = contentResolver.query(Documents.CONTENT_URI, null, Documents.GUID + "='" + guid + "'", null, null);
		if (null != cursor && cursor.moveToNext()) {
			ret = cursor.getInt(cursor.getColumnIndex(Documents.DOCUMENT_ID));
		}
		cursor.close();
		return ret;
	}

	// get the note text from the title of the note
	public void initDataFromDocumentWithId(ContentResolver contentResolver, int id) {
		Cursor cursor = contentResolver.query(Documents.CONTENT_URI, null, Documents.DOCUMENT_ID + "='" + id + "'", null, null);
		if (null != cursor && cursor.moveToNext()) {
			initDataWithCursor(cursor);
		}
		cursor.close();
	}
	
	public void initDataWithCursor(Cursor cursor){
			_id = cursor.getInt(cursor.getColumnIndex(Documents.DOCUMENT_ID));
			_channelId = cursor.getInt(cursor.getColumnIndex(Documents.CHANNEL_ID));
			_title = cursor.getString(cursor.getColumnIndex(Documents.TITLE));
			_category = cursor.getString(cursor.getColumnIndex(Documents.CATEGORY));
			_author = cursor.getString(cursor.getColumnIndex(Documents.AUTHOR));
			_keywords = cursor.getString(cursor.getColumnIndex(Documents.KEYWORDS));
			_summary = cursor.getString(cursor.getColumnIndex(Documents.SUMMARY));
			_legalInfos = cursor.getString(cursor.getColumnIndex(Documents.LEGAL_INFOS));
			_version = cursor.getString(cursor.getColumnIndex(Documents.VERSION));
			_guid = cursor.getString(cursor.getColumnIndex(Documents.GUID));
			_downloadUrl = cursor.getString(cursor.getColumnIndex(Documents.DOWNLOAD_URL));
			_localUri = cursor.getString(cursor.getColumnIndex(Documents.LOCAL_URI));
			_status = cursor.getInt(cursor.getColumnIndex(Documents.STATUS));
			_statusProgress = cursor.getInt(cursor.getColumnIndex(Documents.STATUS_PROGRESS));
			_isFavorite = (cursor.getInt(cursor.getColumnIndex(Documents.IS_FAVORITE)) == 1);
			_lastBuildDate = cursor.getLong(cursor.getColumnIndex(Documents.LAST_BUILD_DATE));
			_publicationDate = cursor.getLong(cursor.getColumnIndex(Documents.PUBLICATION_DATE));
			_lastOpenedPage = cursor.getString(cursor.getColumnIndex(Documents.LAST_OPENED_PAGE));
			_rssPos = cursor.getInt(cursor.getColumnIndex(Documents.RSS_POS));
			_smallThumbLocalUri = cursor.getString(cursor.getColumnIndex(Documents.THUMB_SMALL_LOCAL_URI));
			_smallThumbUrl = cursor.getString(cursor.getColumnIndex(Documents.THUMB_SMALL_URL));
			_largeThumbLocalUri = cursor.getString(cursor.getColumnIndex(Documents.THUMB_LARGE_LOCAL_URI));
			_largeThumbUrl = cursor.getString(cursor.getColumnIndex(Documents.THUMB_LARGE_URL));
			_updateAvailable = (cursor.getInt(cursor.getColumnIndex(Documents.UPDATE_AVAILABLE)) == 1);
			_tmpArchivePath = cursor.getString(cursor.getColumnIndex(Documents.TMP_ARCHIVE_PATH));
			_headerLastModified = cursor.getString(cursor.getColumnIndex(Documents.HEADER_LAST_MODIFIED));
			_normalizedSearchFields =  cursor.getString(cursor.getColumnIndex(Documents.NORMALIZED_SEARCH_FIELD));
			_onlineUrl = cursor.getString(cursor.getColumnIndex(Documents.ONLINE_URL));
			_documentType = cursor.getString(cursor.getColumnIndex(Documents.TYPE));
	}
	
	public void updateDocument(final ContentResolver contentResolver) {
		new Thread(new Runnable() {
	        public void run() {
	        	contentResolver.update(Documents.CONTENT_URI, getContentValues(), Documents.DOCUMENT_ID + "='" + _id + "'", null);
	        }
	    }).start();
		
	}
	
	public void updateThumb(final ContentResolver contentResolver) {
		new Thread(new Runnable() {
	        public void run() {
	        	contentResolver.update(Documents.CONTENT_URI, getContentValuesForThumbUpdate(), Documents.DOCUMENT_ID + "='" + _id + "'", null);
	        }
	    }).start();
		
	}
	
	public void updateDocumentStatus(final ContentResolver contentResolver) {
		MyLogs.write("Update Document Status for document : "+this._title, MyLogs.ERROR);
		new Thread(new Runnable() {
	        public void run() {
	        	contentResolver.update(Documents.CONTENT_URI, getContentValuesForStatusUpdate(), Documents.DOCUMENT_ID + "='" + _id + "'", null);
	        }
	    }).start();
		
	}
	
	public void updateDocumentProgress(final ContentResolver contentResolver) {
		new Thread(new Runnable() {
	        public void run() {
	        	contentResolver.update(Documents.CONTENT_URI, getContentValuesForProgressUpdate(), Documents.DOCUMENT_ID + "='" + _id + "'", null);
	        }
	    }).start();
	}
	
	public void removeFromShelves(ContentResolver contentResolver) {
		_status = STATUS_NOT_DOWNLOADED;
		updateDocumentStatus(contentResolver);

		File file = new File(_localUri);
		if (file.exists()) 
			deleteRecursive(file);
		
		file = new File(_tmpArchivePath);
		if (file.exists()) 
			deleteRecursive(file);
	}
	
	public void deleteData(ContentResolver contentResolver) {
		File file = new File(_localUri);
		if (file.exists()) 
			deleteRecursive(file);
		
		file = new File(_tmpArchivePath);
		if (file.exists()) 
			deleteRecursive(file);
		
		contentResolver.delete(Documents.CONTENT_URI,  Documents.DOCUMENT_ID + "='" + _id + "'", null);
	}
	
	public void addRemoveFav(ContentResolver contentResolver){
		_isFavorite = (!_isFavorite);
		updateDocument(contentResolver);
	}
	
	void deleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles())
				deleteRecursive(child);
		fileOrDirectory.delete();
	}
	
	// erases all entries in the database
	public void refreshCache(ContentResolver contentResolver) {
//		int delete = contentResolver.delete(Notes.CONTENT_URI, null, null);
//		System.out.println("DELETED " + delete + " RECORDS FROM CONTACTS DB");
	}

	@Override
	public int describeContents() {
		return 0;
	}
	
	public static final Parcelable.Creator<Document> CREATOR = new Parcelable.Creator<Document>() {
		public Document createFromParcel(Parcel in) {
			return new Document(in);
		}

		public Document[] newArray(int size) {
			return new Document[size];
		}
	};
	
	private Document(Parcel in) {
		readFromParcel(in);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(_id);
		dest.writeInt(_channelId);
		dest.writeString(_title);
		dest.writeString(_category);
		dest.writeString(_author);
		dest.writeString(_keywords);
		dest.writeString(_summary);
		dest.writeString(_legalInfos);
		dest.writeString(_version);
		dest.writeString(_guid);
		dest.writeString(_downloadUrl);
		dest.writeString(_localUri);
		dest.writeInt(_status);
		dest.writeInt(_statusProgress);
		dest.writeByte((byte) (_isFavorite ? 1 : 0));
		dest.writeLong(_lastBuildDate);
		dest.writeLong(_publicationDate);
		dest.writeString(_lastOpenedPage);
		dest.writeInt(_rssPos);
		dest.writeString(_smallThumbLocalUri);
		dest.writeString(_smallThumbUrl);
		dest.writeString(_largeThumbLocalUri);
		dest.writeString(_largeThumbUrl);
		dest.writeByte((byte) (_updateAvailable ? 1 : 0));
		dest.writeString(_tmpArchivePath);
		dest.writeString(_headerLastModified);
		dest.writeString(_onlineUrl);
		dest.writeString(_documentType);
		dest.writeString(_normalizedSearchFields);
	}

	public void readFromParcel(Parcel in) {
		_id = in.readInt();
		_channelId = in.readInt();
		_title = in.readString();
		_category = in.readString();
		_author = in.readString();
		_keywords = in.readString();
		_summary = in.readString();
		_legalInfos = in.readString();
		_version = in.readString();
		_guid = in.readString();
		_downloadUrl = in.readString();
		_localUri = in.readString();
		_status = in.readInt();
		_statusProgress = in.readInt();
		_isFavorite = in.readByte() == 1;
		_lastBuildDate = in.readLong();
		_publicationDate = in.readLong();
		_lastOpenedPage = in.readString();
		_rssPos = in.readInt();
		_smallThumbLocalUri = in.readString();
		_smallThumbUrl = in.readString();
		_largeThumbLocalUri = in.readString();
		_largeThumbUrl = in.readString();
		_updateAvailable = in.readByte() == 1;
		_tmpArchivePath = in.readString();
		_headerLastModified = in.readString();
		_onlineUrl = in.readString();
		_documentType = in.readString();
		_normalizedSearchFields = in.readString();
	}
}
