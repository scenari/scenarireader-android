package com.soreha.scenari_reader.lib.data;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

import com.soreha.scenari_reader.lib.util.MyLogs;

public class Bookmark {

	public Bookmark() {
	}

	public static final class Bookmarks implements BaseColumns {
		private Bookmarks() {
		}

		public static final Uri CONTENT_URI = Uri.parse("content://" + DataContentProvider.AUTHORITY + "/bookmarks");

		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.jwei512.bookmarks";
		public static final String BOOKMARK_ID = "_id";
		public static final String DOCUMENT_ID = "_documentId";
		public static final String PAGE_URI = "_pageURI";
		public static final String PAGE_TITLE = "_pageTitle";
	}
	
	public static final class BookmarksInfos implements BaseColumns {
		private BookmarksInfos() {
		}

		public static final Uri CONTENT_URI = Uri.parse("content://" + DataContentProvider.AUTHORITY + "/bookmarksInfos");

		public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.jwei512.bookmarksInfos";
		public static final String BOOKMARK_ID = "_id";
		public static final String DOCUMENT_ID = "_documentId";
		public static final String PAGE_URI = "_pageURI";
		public static final String PAGE_TITLE = "_pageTitle";
		public static final String DOCUMENT_TITLE = "title";
		public static final String CHANNEL_TITLE = "ch_title";
	}

	// adds a new note with a given title and text
	public static void addBookmark(ContentResolver contentResolver,int docId, String pageURI,String pageTitle) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(Bookmarks.DOCUMENT_ID, docId);
		contentValues.put(Bookmarks.PAGE_URI, pageURI);
		contentValues.put(Bookmarks.PAGE_TITLE, pageTitle);
		contentResolver.insert(Bookmarks.CONTENT_URI, contentValues);
		MyLogs.d("Bookmark added");
	}
	
	public static void removeBookmark (ContentResolver contentResolver,int docId,String pageURI){
		contentResolver.delete(Bookmarks.CONTENT_URI, Bookmarks.DOCUMENT_ID + "='" + docId + "' AND " +  Bookmarks.PAGE_URI + "= '" + pageURI + "'", null);
	}
	
	public static boolean isPageBookmarked(ContentResolver contentResolver,int docId, String pageURI){
		int ret = -1;
		Cursor cursor = contentResolver.query(Bookmarks.CONTENT_URI, null, Bookmarks.DOCUMENT_ID + "='" + docId + "' AND " +  Bookmarks.PAGE_URI + "= '" + pageURI + "'", null, null);
		if (null != cursor && cursor.moveToNext()) {
			ret = cursor.getInt(cursor.getColumnIndex(Bookmarks.BOOKMARK_ID));
		}
		cursor.close();
		
		if (ret == -1)
			return false;
		else
			return true;
	}
}
