package com.soreha.scenari_reader.lib.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Formatter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.ListView;

/**
 * Cette classe regroupe en ensemble d'utilitaires
 * 
 * @author Mory Doukour� - Soreha
 * 
 */
public class Utils {

	/**
	 * V�rification du statut de la connexion internet
	 * 
	 * @param context
	 *            Context de l'activit�
	 * @return True si une connexion est possible, false sinon
	 */
	public static boolean checkNetworkConnection(Context context) {
		boolean isActive = false;
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo[] nf = cm.getAllNetworkInfo();

		for (int i = 0; i < nf.length; i++) {
			if (nf[i].isConnected()) {
				isActive = true;
			}
		}
		return isActive;
	}

	/**
	 * V�rification de l'�tat des services de localisation
	 * 
	 * @param context
	 *            Context de l'applicat
	 * @return Etat des service de localisation
	 */
	public static boolean isLocationServiceEnable(Context context) {
		LocationManager locMngr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		if (locMngr.isProviderEnabled(LocationManager.GPS_PROVIDER) || locMngr.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
			return true;
		else 
			return false;
	}

	/**
	 * Permet l'affichage d'une boite de dialog standart
	 * 
	 * @param activity
	 *            Activit� m�re
	 * @param title
	 *            Titre de la dialogue
	 * @param message
	 *            Message � afficher
	 */
	public static void showStandardAlertDialog(Activity activity, String title, String message) {
		showAlertDialog(activity, title, message, "Fermer", "", null, null, null, null, false);
	}

	/**
	 * Permet l'affichage d'une boite de dialog qui terminera l'activit� en
	 * cours � sa fermeture
	 * 
	 * @param activity
	 *            Activit� m�re
	 * @param title
	 *            Titre de la dialogue
	 * @param message
	 *            Message � afficher
	 */
	public static void showFinishingAlertDialog(Activity activity, String title, String message) {
		showAlertDialog(activity, title, message, "Fermer", "", null, null, null, null, true);
	}
	
	public static int getCheckedItemCount (ListView listview){
		int counter = 0;
		int size = listview.getAdapter().getCount();
		for(int i = 0; i < size; i++) {
		  if(listview.getCheckedItemPositions().get(i))
		    counter++;
		}
		return counter;
	}

	/**
	 * Permet l'affichage d'une boite de dialog qui executera une m�thode au
	 * moment du clique les boutons
	 * 
	 * @param activity
	 *            Activit� m�re
	 * @param title
	 *            Titre de la dialogue
	 * @param message
	 *            Message de la dialogue
	 * @param pButtonText
	 *            Texte bouton positif
	 * @param nButtonText
	 *            Texte bouton n�gatif
	 * @param pMethod
	 *            M�thode associ� au bouton positif
	 * @param nMethod
	 *            M�thode associ� au bouton n�gatif
	 * @param pMethodReceiver
	 *            Receveur de la m�thode du bouton positif
	 * @param nMethodReceiver
	 *            Receveur de la m�thode du bouton n�gatif
	 * @param isCritical
	 *            Si true, fermeture de l'activit� � la fermeture de la dialogue
	 */
	public static void showAlertDialog(final Activity activity, final String title, final String message, final String pButtonText, final String nButtonText,
			final Method pMethod, final Method nMethod, final Object pMethodReceiver, final Object nMethodReceiver, final boolean isCritical) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle(title);
		builder.setMessage(message);

		builder.setPositiveButton(pButtonText, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				if (pMethod != null) {
					try {
						pMethod.invoke(pMethodReceiver, (Object[])null);
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (isCritical)
					activity.finish();
			}
		});

		if (!nButtonText.equals("")) {
			builder.setNegativeButton(nButtonText, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
					if (nMethod != null) {
						try {
							nMethod.invoke(nMethodReceiver, (Object[])null);
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if (isCritical)
						activity.finish();
				}
			});
		}
		builder.show();
	}
	
	public static String formatString(String message, Object...vars){
		StringBuilder sb = new StringBuilder();
		Formatter formatter = new Formatter(sb);
		formatter.format(message,vars);
		return sb.toString();
	}
}
