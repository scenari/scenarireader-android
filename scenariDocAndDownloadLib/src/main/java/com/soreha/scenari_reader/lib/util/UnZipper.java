package com.soreha.scenari_reader.lib.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Observable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.IOUtils;

import android.os.AsyncTask;
import android.util.Log;

import com.soreha.scenari_reader.lib.data.Document;

public class UnZipper extends Observable {

	private static final String TAG = "UnZip";
	private String _filePath, _destinationPath;
	private Document _document;
	private int _nbEntries, _currentEntries, _progress;
	
	public UnZipper(Document document) {
		_filePath = document._tmpArchivePath;
		_destinationPath = document._tmpFolderPath;
		_document = document;
	}

	public String getFilePath() {
		return _filePath;
	}

	public String getDestinationPath() {
		return _destinationPath;
	}

	public void unzip() {
		String fullPath = _filePath;
		MyLogs.d("unzipping " + _filePath + " to " + _destinationPath);
		new UnZipTask().execute(fullPath, _destinationPath);
	}

	private class UnZipTask extends AsyncTask<String, Integer, Boolean> {

		@SuppressWarnings("rawtypes")
		@Override
		protected Boolean doInBackground(String... params) {
			String filePath = params[0];
			String destinationPath = params[1];
			
			File archive = new File(filePath);
			try {
				ZipFile zipfile = new ZipFile(archive);
				Enumeration e;
				_progress = 0;
				_nbEntries = 0;
				_currentEntries = 0;
				for (e = zipfile.entries(); e.hasMoreElements();) {
					 e.nextElement();
					_nbEntries ++;
				}
				for (e = zipfile.entries(); e.hasMoreElements();) {
					int progress = (int)((_currentEntries*100) / _nbEntries );
					ZipEntry entry = (ZipEntry) e.nextElement();
					unzipEntry(zipfile, entry, destinationPath);
					
					_currentEntries ++;

					if (progress >= (_progress + 5)){
						_progress = progress;
						publishProgress(progress);
					}
				}
			} catch (Exception e) {
				Log.e(TAG, "Error while extracting file " + archive, e);
				return false;
			}
			_progress = 100;
			publishProgress(_progress);
			return true;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			MyLogs.write("Extract progress : "+values[0].toString()+"%",MyLogs.DEBUG);
			_document._statusProgress = values[0];
			setChanged();
			notifyObservers(_document);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			setChanged();
			if (result){
				_document._status = Document.STATUS_EXTRACTED;
				notifyObservers(_document);
			}
			else{
				_document._status = Document.STATUS_EXTRACT_ERROR;
				notifyObservers(_document);
			}
		}

		private void unzipEntry(ZipFile zipfile, ZipEntry entry, String outputDir) throws IOException {

			if (entry.isDirectory()) {
				createDir(new File(outputDir, entry.getName()));
				return;
			}

			File outputFile = new File(outputDir, entry.getName());
			if (!outputFile.getParentFile().exists()) {
				createDir(outputFile.getParentFile());
			}

			//Log.v(TAG, "Extracting: " + entry);
			BufferedInputStream inputStream = new BufferedInputStream(zipfile.getInputStream(entry));
			BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));

			try {
				IOUtils.copy(inputStream, outputStream);
			} catch (Exception e) {
				e.printStackTrace();
			}
			outputStream.flush();
				outputStream.close();
				inputStream.close();
			
		}

		private void createDir(File dir) {
			if (dir.exists()) {
				return;
			}
			//Log.v(TAG, "Creating dir " + dir.getName());
			if (!dir.mkdirs()) {
				throw new RuntimeException("Can not create dir " + dir);
			}
		}
	}
}