package com.soreha.scenari_reader.lib.util;


import com.soreha.scenari_reader.lib.BuildConfig;

import android.util.Log;

/** Gestion de l'affichage ou non des logs */
public final class MyLogs {
	private static final String LOG_TAG = "OPALE";
	public static final int DEBUG = 0;
	public static final int INFO = 1;
	public static final int ERROR = 2;
	public static final int TITLE = 3;

	@SuppressWarnings("unused")
	public static void write(String tag, String message, int type) {
		
		if (BuildConfig.DEBUG)
			if (type == DEBUG)
				Log.d(tag, message);
			else if (type == INFO)
				Log.i(tag, message);
			else if (type == ERROR)
				Log.e(tag, message);
			else
				Log.i(tag, message);
	}
	
	public static void write(String message, int type) {
		
		if (BuildConfig.DEBUG)
			if (type == TITLE)
				Log.e(LOG_TAG, "************"+ message+"***************");
			else if (type == DEBUG)
				Log.d(LOG_TAG, message);
			else if (type== ERROR)
				Log.e(LOG_TAG, message);
			else if (type == INFO)
				Log.i(LOG_TAG, message);
			else 
				Log.i(LOG_TAG, message);
		
	}
	
	public static void d(String tag, String message) {
		if (BuildConfig.DEBUG)
			Log.d(tag, message);
	}
	
	public static void d(String message) {
		d(LOG_TAG, message);
	}

	public static void e(String tag, String message) {
		if (BuildConfig.DEBUG)
			Log.e(LOG_TAG, message);
	}

	public static void e(String message) {
		e(LOG_TAG, message);
	}

	public static void i(String message) {
		i(LOG_TAG, message);
	}

	public static void i(String tag, String message) {
		if (BuildConfig.DEBUG)
			Log.i(tag, message);
	}

	public static void t(String tag, String message) {
		if (BuildConfig.DEBUG)
			Log.e(tag, "************" + message + "***************");
	}

	public static void t(String message) {
		e(LOG_TAG, "************" + message + "***************");
	}
}
