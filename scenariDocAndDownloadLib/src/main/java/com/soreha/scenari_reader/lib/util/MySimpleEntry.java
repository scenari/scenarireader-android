package com.soreha.scenari_reader.lib.util;

import java.util.Map;

public class MySimpleEntry<K, V> implements Map.Entry<K, V> {
    private final K key;
    private V value;

    public MySimpleEntry(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public V setValue(V value) {
        V old = this.value;
        this.value = value;
        return old;
    }
}