package com.soreha.scenari_reader;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.webkit.DownloadListener;
import android.webkit.MimeTypeMap;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.soreha.scenari_reader.R.id;
import com.soreha.scenari_reader.dialogs.DialogFragment_ListBookmarks;
import com.soreha.scenari_reader.lib.data.Bookmark;
import com.soreha.scenari_reader.lib.data.Document;
import com.soreha.scenari_reader.lib.util.MyLogs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Pattern;

public class Activity_Reader extends AppCompatActivity {

	private ImageView _ivBookmark;
	private Animation _anim_out, _anim_in;
	private Document _document;
	private boolean _isPageBookmarked = false;
	private String _currentPageTitle = "";
	private FragmentManager _fragmentManager;
	public WebView _webView;
	private ActionBar _actionBar;
	boolean _fromLocal = true;
	private String _domain = null;
	public boolean _isLargeScreen = false;
	public ImageView _ivWebView;

	public FrameLayout mContentView;
	public FrameLayout mCustomViewContainer;
	public View mCustomView;
	public WebChromeClient.CustomViewCallback mCustomViewCallback;
	public String _lastPageStarted = "";

	@SuppressLint("SetJavaScriptEnabled")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_reader);
		_ivWebView = (ImageView) findViewById(id.iv_webview);
		mContentView = (FrameLayout) findViewById(R.id.main_content);
		mCustomViewContainer = (FrameLayout) findViewById(R.id.fullscreen_custom_content);

		_isLargeScreen = getIntent().getBooleanExtra("isLarge", false);

		_actionBar = getSupportActionBar();
		_actionBar.setDisplayHomeAsUpEnabled(true);

		Bundle b = getIntent().getExtras();

		_document = b.getParcelable("document");
		boolean fromLastPage = b.getBoolean("fromLastPage", false);
		_fromLocal = b.getBoolean("fromLocal", true);

		_actionBar.setTitle(_document._title/* +" - "+doc._version */);
		_actionBar.setSubtitle(_document._author);

		_fragmentManager = getSupportFragmentManager();

		_webView = (WebView) findViewById(R.id.webView1);

		_webView.setVerticalScrollBarEnabled(false);
		_webView.setVerticalFadingEdgeEnabled(false);
		_webView.getSettings().setDomStorageEnabled(true);
		//_webView.getSettings().setDatabasePath("/data/data/"+getApplicationContext().getPackageName()+"/databases/");
		_webView.getSettings().setJavaScriptEnabled(true);
		_webView.getSettings().setPluginState(PluginState.ON);
		_webView.getSettings().setAllowFileAccess(true);
		if (Build.VERSION.SDK_INT >= 16)
			_webView.getSettings().setAllowFileAccessFromFileURLs(true);
		_webView.getSettings().setSupportZoom(false);
		_webView.getSettings().setRenderPriority(RenderPriority.HIGH);
		_webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		_webView.setOverScrollMode(View.OVER_SCROLL_NEVER);
		if (Build.VERSION.SDK_INT >= 16)
			_webView.getSettings().setAllowFileAccessFromFileURLs(true);

		_webView.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onReceivedTitle(WebView view, String sTitle) {
				super.onReceivedTitle(view, sTitle);
				if (sTitle != null && sTitle.length() > 0) {

				}
			}

			@Override
			public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback)
			{
				// if a view already exists then immediately terminate the new
				// one
				if (mCustomView != null)
				{
					callback.onCustomViewHidden();
					return;
				}

				// Add the custom view to its container.
				mCustomViewContainer.addView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
				mCustomView = view;
				mCustomViewCallback = callback;

				// hide main browser view
				mContentView.setVisibility(View.INVISIBLE);

				// Finally show the custom view container.
				mCustomViewContainer.setVisibility(View.VISIBLE);
				mCustomViewContainer.bringToFront();
			}

			@Override
			public void onHideCustomView() {
				Activity_Reader.this.onHideCustomView();
			}

			// public Bitmap getDefaultVideoPoster()
			// {
			// if (mDefaultVideoPoster == null)
			// {
			// mDefaultVideoPoster =
			// BitmapFactory.decodeResource(getResources(),
			// R.drawable.default_video_poster);
			// }
			// return mDefaultVideoPoster;
			// }
			//
			// public View getVideoLoadingProgressView()
			// {
			// if (mVideoProgressView == null)
			// {
			// LayoutInflater inflater = LayoutInflater.from(this);
			// mVideoProgressView =
			// inflater.inflate(R.layout.video_loading_progress, null);
			// }
			// return mVideoProgressView;
			// }
		});

		_webView.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				MyLogs.d("ShouldOverride URL : " + _fromLocal + " - " + url);
				if (url == null || url.equals("about:blank")) {
					return true;
				} else {

					String domain = _domain;
					try {
						domain = new URI(url).getHost();
					} catch (URISyntaxException e) {
						e.printStackTrace();
					}

					MyLogs.d("URL : " + domain + " - " + _domain);

					if (url.startsWith("file")) {
						if (!url.endsWith("html")) {
							try
							{
								Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.setDataAndType(Uri.parse(url), getMimeType(url));
								startActivity(intent);
							}
							catch (Exception e)
							{
								getUnkownLocalFile(url);
							}
							return true;
						}
						return false;
					} else if (!domain.equals(_domain)) {
						Intent intent = new Intent(Intent.ACTION_VIEW);
						intent.setData(Uri.parse(url));
						startActivity(intent);
						return true;
					} else if (!url.endsWith("html")) {
						downloadFile(url);
						return true;
					} else {
						return false;
					}
				}
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				MyLogs.d("Page started url : " + url);
				Activity_Reader.this.setProgressBarIndeterminateVisibility(true);
				/*View v = findViewById(id.rl_reader_main_layout);
				if (_document._documentType.equals(Document.DOCUMENT_TYPE_OPALE) && v.getWidth() > 0 && v.getHeight() > 0) {
					Bitmap image = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.RGB_565);
					v.draw(new Canvas(image));

					_ivWebView.setImageBitmap(image);
					_ivWebView.setVisibility(View.VISIBLE);
				}*/
				super.onPageStarted(view, url, favicon);
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				MyLogs.d("Page finished url : " + url);
				super.onPageFinished(view, url);
				if (!Pattern.matches(".*?html#.*", url)) {
					if (url.startsWith("file") || !_fromLocal) {
						if (!(_isPageBookmarked = Bookmark.isPageBookmarked(getContentResolver(), _document._id, url)))
							_ivBookmark.setVisibility(View.INVISIBLE);
						else
							_ivBookmark.setVisibility(View.VISIBLE);
						_document._lastOpenedPage = url;
						_currentPageTitle = view.getTitle();
						_document.updateDocument(getContentResolver());
					}
					Activity_Reader.this.setProgressBarIndeterminateVisibility(false);

					/*Animation out = AnimationUtils.loadAnimation(Activity_Reader.this, anim.fade_out);
					out.setInterpolator(Activity_Reader.this, android.R.anim.accelerate_interpolator);
					out.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
						}

						@Override
						public void onAnimationEnd(Animation animation) {
							_ivWebView.setVisibility(View.INVISIBLE);
						}
					});
					_ivWebView.startAnimation(out);*/
				}
			}
		});

		_webView.setDownloadListener(new DownloadListener() {
			@Override
			public void onDownloadStart(String url, String userAgent, String
					contentDisposition, String mimetype, long contentLength) {
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(url));
				startActivity(intent);
			}
		});

		try {
			if (_fromLocal) {
				MyLogs.d("From Local - domain : " + _domain);
				if (fromLastPage && !_document._lastOpenedPage.equals("") && new URI(_document._lastOpenedPage).getHost() == null)
					_webView.loadUrl(_document._lastOpenedPage);
				else
					_webView.loadUrl("file://" + _document._localUri + "/index.html");
			} else {
				_domain = new URI(_document._onlineUrl).getHost();
				if (!fromLastPage)
					_webView.loadUrl(_document._onlineUrl);
				else
					_webView.loadUrl(_document._lastOpenedPage);
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		_ivBookmark = (ImageView) findViewById(R.id.act_reader_iv_bookmark);
		_anim_out = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_out);
		_anim_out.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				_ivBookmark.setVisibility(View.INVISIBLE);
			}
		});

		_anim_in = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_in);
		_anim_in.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				_ivBookmark.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
			}
		});
	}

	@Override
	public void onBackPressed() {
		if (mCustomView != null) {
			onHideCustomView();
		} else {
			super.onBackPressed();
		}
	}

	public void onHideCustomView() {
		if (mCustomView == null)
			return;

		// Hide the custom view.
		mCustomView.setVisibility(View.GONE);
		// Remove the custom view from its container.
		mCustomViewContainer.removeView(mCustomView);
		mCustomView = null;
		mCustomViewContainer.setVisibility(View.GONE);
		mCustomViewCallback.onCustomViewHidden();

		// Show the content view.
		mContentView.setVisibility(View.VISIBLE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mCustomView != null)
		{
			if (mCustomViewCallback != null)
				mCustomViewCallback.onCustomViewHidden();
			mCustomView = null;
		}
		super.onStop();
	}

	@SuppressLint("NewApi")
	public void getUnkownLocalFile(String url) {
		File file = new File(url.substring(url.indexOf(":") + 2));
		if (file.exists()) {
			DownloadManager mgr = (DownloadManager) getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
			File downloadFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

			File newFile = new File(downloadFolder.getAbsolutePath() + File.separator + file.getName());
			int i = 1;
			while (newFile.exists()) {
				String newName = file.getName().substring(0, file.getName().lastIndexOf(".")) + "-" + i
						+ file.getName().substring(file.getName().lastIndexOf("."));
				newFile = new File(downloadFolder.getAbsolutePath() + File.separator + newName);
				i++;
			}

			file.renameTo(newFile);

			(new AsyncTask<File, Void, Void>() {
				@Override
				protected Void doInBackground(File... params) {
					InputStream in;
					try {
						in = new FileInputStream(params[0]);

						OutputStream out = new FileOutputStream(params[1]);

						// Transfer bytes from in to out
						byte[] buf = new byte[1024];
						int len;
						while ((len = in.read(buf)) > 0) {
							out.write(buf, 0, len);
						}
						in.close();
						out.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;
				}
			}).execute(newFile, file);

			MyLogs.d(file.getAbsolutePath());

			if (android.os.Build.VERSION.SDK_INT > 11)
				mgr.addCompletedDownload(newFile.getName(), getString(R.string.reader_downloadedFile_title), false, getMimeType(url),
						newFile.getAbsolutePath(), newFile.length(),
						true);

			AlertDialog.Builder builder = new AlertDialog.Builder(Activity_Reader.this);
			builder.setTitle(R.string.alert_dialog_title_infos);
			String message = getString(R.string.reader_open_downloadedDoc)
					+ downloadFolder.getName();

			if (android.os.Build.VERSION.SDK_INT > 11) {
				message += getString(R.string.reader_open_downloadManager);

				builder.setPositiveButton(R.string.alert_dialog_oui, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent i = new Intent();
						i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
						startActivity(i);
					}
				});

				builder.setNegativeButton(R.string.alert_dialog_non, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
			} else {
				builder.setNegativeButton(R.string.alert_dialog_close, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
			}

			builder.setMessage(message);
			builder.show();
		}
	}

	@SuppressLint("NewApi")
	public void downloadFile(String url) {
		File downloadFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		File remoteFile = new File(url);

		DownloadManager mgr = (DownloadManager) getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
		boolean isDownloading = false;
		DownloadManager.Query query = new DownloadManager.Query();
		query.setFilterByStatus(
				DownloadManager.STATUS_PAUSED |
						DownloadManager.STATUS_PENDING |
						DownloadManager.STATUS_RUNNING);
		Cursor cur = mgr.query(query);
		int col = cur.getColumnIndex(
				DownloadManager.COLUMN_LOCAL_FILENAME);
		for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
			MyLogs.d(downloadFolder.getAbsolutePath() + File.separator + remoteFile.getName() + " - " + cur.getString(col));
			isDownloading = ((downloadFolder.getAbsolutePath() + File.separator + remoteFile.getName()).equals(cur.getString(col)));
		}
		cur.close();

		if (!isDownloading) {
			MyLogs.t("Start download for document " + remoteFile.getName());
			Uri source = Uri.parse(url);
			Uri destination = Uri.fromFile(new File(downloadFolder.getAbsolutePath() + File.separator + remoteFile.getName()));

			DownloadManager.Request request =
					new DownloadManager.Request(source);
			request.setTitle(remoteFile.getName());
			request.setDestinationUri(destination);
			if (android.os.Build.VERSION.SDK_INT > 10)
				request.setNotificationVisibility(DownloadManager
						.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

			mgr.enqueue(request);
		}

		showDownloadAlertDialog();
	}

	public void showDownloadAlertDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.alert_dialog_title_infos);
		builder.setMessage(R.string.alert_dialog_message_show_downloadManager2);
		builder.setPositiveButton(R.string.alert_dialog_oui, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				Intent i = new Intent();
				i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
				startActivity(i);
			}
		});

		builder.setNegativeButton(R.string.alert_dialog_non, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.show();
	}

	public static String getMimeType(String url)
	{
		String type = null;
		String extension = MimeTypeMap.getFileExtensionFromUrl(url);
		if (extension != null) {
			MimeTypeMap mime = MimeTypeMap.getSingleton();
			type = mime.getMimeTypeFromExtension(extension);
		}
		return type;
	}

	// Creation du menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (_document._documentType.equals(Document.DOCUMENT_TYPE_OPALE))
		{
			super.onCreateOptionsMenu(menu);
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.menu_reader, menu);
			return true;
		} else {
			return super.onCreateOptionsMenu(menu);
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in action bar clicked; go home
			finish();
			return true;
		case R.id.add_remove_fav:
			if (!_isPageBookmarked) {
				_ivBookmark.startAnimation(_anim_in);
				Toast.makeText(getApplicationContext(), R.string.reader_toast_bookmard_added, Toast.LENGTH_SHORT).show();
				_isPageBookmarked = true;
				Bookmark.addBookmark(getContentResolver(), _document._id, _document._lastOpenedPage, _currentPageTitle);
			} else {
				_ivBookmark.startAnimation(_anim_out);
				Toast.makeText(getApplicationContext(), R.string.reader_toast_bookmark_removed, Toast.LENGTH_SHORT).show();
				_isPageBookmarked = false;
				Bookmark.removeBookmark(getContentResolver(), _document._id, _document._lastOpenedPage);
			}
			return true;
		case R.id.fav_list:
			showBookmarkListDialog();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}



	public void showBookmarkListDialog() {
		Intent intent = new Intent(this,DialogFragment_ListBookmarks.class);
		intent.putExtra("documentId",_document._id);
		startActivityForResult(intent,0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			_webView.loadUrl(data.getStringExtra("url"));
		}
	}
}
