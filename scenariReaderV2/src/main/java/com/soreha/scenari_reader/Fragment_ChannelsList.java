package com.soreha.scenari_reader;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.soreha.scenari_reader.R.string;
import com.soreha.scenari_reader.lib.data.Channel;
import com.soreha.scenari_reader.lib.data.Channel.Channels;
import com.soreha.scenari_reader.lib.util.MyLogs;
import com.soreha.scenari_reader.lib.util.Utils;

import java.util.HashMap;


public class Fragment_ChannelsList extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
	public static final int LOADER_CHANNEL_ID = -5555;
	public AdapterView<BaseAdapter> _channelsList;
	public ChannelCursorAdapter _channelCursorAdapter;
	private boolean _channelSelected = false;
	private boolean _channelDeleted = false;
	private int _deletedChannelPos = 0;
	private boolean _isLargeScreen = false;
	public boolean _isChannelMenuOpened = false;
	private ChannelListener _listener;
	private boolean _channelsChecked = false;

	public Context _context;

	public static Fragment_ChannelsList newInstance() {

		Bundle args = new Bundle();

		Fragment_ChannelsList fragment = new Fragment_ChannelsList();
		fragment.setArguments(args);
		return fragment;
	}
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		_isLargeScreen = getResources().getBoolean(R.bool.isLargeScreen);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		MyLogs.write("Fragment View Created", MyLogs.TITLE);
		return inflater.inflate(R.layout.fragment_channels_list, container, false);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		_context = activity.getApplicationContext();
		_listener = (ChannelListener) activity;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		_channelCursorAdapter = new ChannelCursorAdapter(_context, null, 0);

		_channelsList = (AdapterView<BaseAdapter>) view.findViewById(R.id.horizontal_listview);

		_channelsList.setAdapter(_channelCursorAdapter);
		_channelsList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if ((!_isLargeScreen && arg2 == 0) || (_isLargeScreen && arg2 >= (_channelCursorAdapter.getCount() - 1)))
					_listener.openAddChannelDialog(null);
				else {
					if (!_isChannelMenuOpened) {
						_channelsList.setSelection(arg2);
						MyLogs.d("Item selected : " + arg2 + " - " + _channelsList.getSelectedItemPosition());
						_channelCursorAdapter.notifyDataSetChanged();

						Cursor cursor;
						if (_isLargeScreen)
							cursor = (Cursor) _channelCursorAdapter.getItem(arg2);
						else
							cursor = (Cursor) _channelCursorAdapter.getItem(arg2 - 1);

						int channelId = cursor.getInt(cursor.getColumnIndex(Channels.CHANNEL_ID));
						String channelName = (cursor.getString(cursor.getColumnIndex(Channels.USER_TITLE)).equals("") ? cursor.getString(cursor
								.getColumnIndex(Channels.TITLE)) : cursor.getString(cursor.getColumnIndex(Channels.USER_TITLE)));

						onClickChannel(channelId, channelName);
					}
				}
			}
		});

		_channelsList.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				if ((!_isLargeScreen && arg2 > 0) || (_isLargeScreen && arg2 < (_channelCursorAdapter.getCount() - 1))) {
					Cursor cursor = _channelCursorAdapter.getCursor();
					if (_isLargeScreen)
						cursor.moveToPosition(arg2);
					else
						cursor.moveToPosition(arg2 - 1);
					Channel channel = new Channel(getActivity().getApplicationContext());
					channel.initDataFromCursor(cursor);
					_listener.showChannelPopupMenu(channel, arg1);
					return true;
				}
				return false;
			}
		});

		_channelsList.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		getLoaderManager().initLoader(LOADER_CHANNEL_ID, null, this);
	}


	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	public void setChannelsCursor(Cursor cursor) {
		_channelCursorAdapter.swapCursor(cursor);
		if (!_channelSelected && cursor.getCount() >= 1) {
			_channelSelected = true;
			_channelsList.setSelection(0);
			_channelCursorAdapter.notifyDataSetChanged();
			Cursor c = (Cursor) _channelCursorAdapter.getItem(0);
			int channelId = c.getInt(c.getColumnIndex(Channels.CHANNEL_ID));
			String channelName = (c.getString(c.getColumnIndex(Channels.USER_TITLE)).equals("") ? c.getString(c.getColumnIndex(Channels.TITLE)) : c.getString(c
					.getColumnIndex(Channels.USER_TITLE)));
			if (_isLargeScreen)
				onClickChannel(channelId, channelName);
		}

		if (_isLargeScreen && _channelDeleted) {
			if (cursor.getCount() >= 1) {
				int newSelectedItemPos = 0;
				if (_deletedChannelPos > 1) {
					newSelectedItemPos = _deletedChannelPos - 1;
					_channelsList.setSelection(newSelectedItemPos);
					_channelCursorAdapter.notifyDataSetChanged();
				}
				Cursor c = (Cursor) _channelCursorAdapter.getItem(newSelectedItemPos);
				int channelId = c.getInt(c.getColumnIndex(Channels.CHANNEL_ID));
				String channelName = (c.getString(c.getColumnIndex(Channels.USER_TITLE)).equals("") ? c.getString(c.getColumnIndex(Channels.TITLE)) : c
						.getString(c.getColumnIndex(Channels.USER_TITLE)));

				onClickChannel(channelId, channelName);
			}
			_channelDeleted = false;
		}
	}

	public void onClickChannel(int channelId, String channelName) {
		if (!_isLargeScreen)
			_listener.openDocumentsListFragmentForChannelId(channelId, channelName);
		else
			_listener.initDocumentsLoaderForChannelId(channelId, channelName);
	}

	public void onLongClickChannel(int channelId) {
		// TODO Auto-generated method stub
	}

	public void updateProgress(int progress, String tag) {
		// TODO Auto-generated method stub
	}


	public void channelDeleted() {
		if (_isLargeScreen) {
			_deletedChannelPos = _channelsList.getSelectedItemPosition();
			_channelDeleted = true;
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(_context, Channels.CONTENT_URI, null, null, null, Channels.CHANNEL_ID);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		if (!_channelsChecked) {
			while (data.moveToNext()) {
				Channel channel = new Channel(_context);
				channel.initDataFromCursor(data);
				_listener.checkRssForChannel(channel);
			}
			_channelsChecked = true;
		}

		setChannelsCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {

	}

	public interface ChannelListener {
		void checkRssForChannel(Channel channel);
		void openAddChannelDialog(Channel channel);
		void showChannelPopupMenu(Channel channel, View anchorView);
		void openDocumentsListFragmentForChannelId(int channelId, String channelName);
		void initDocumentsLoaderForChannelId(int channelId, String channelName);
	}

	public class ChannelCursorAdapter extends CursorAdapter {
		private LayoutInflater li;

		public ChannelCursorAdapter(Context context, Cursor c, int flag) {
			super(context, c, flag);
			li = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return super.getCount() + 1;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup arg2) {
			if (_isLargeScreen && position >= super.getCount()) {
				View addChannelView = li.inflate(R.layout.item_list_add_channel, null);
				addChannelView.setTag("addChannelView");
				return addChannelView;
			}
			else if (!_isLargeScreen && position == 0) {
				View addChannelView = li.inflate(R.layout.item_list_add_channel, null);
				addChannelView.setTag("addChannelView");
				return addChannelView;
			}

			final Cursor cursor;
			if (_isLargeScreen)
				cursor = (Cursor) this.getItem(position);
			else
				cursor = (Cursor) this.getItem(position - 1);

			final View view;
			if (convertView == null || convertView.getTag() != null) {
				view = li.inflate(R.layout.item_sv_channel, null);
			} else {
				view = convertView;
			}

			final ImageView iv = (ImageView) view.findViewById(R.id.iv_channel_thumb);
			Bitmap bm = Fragment_ChannelsList.this.getImageForChannel(cursor);
			if (bm != null)
				iv.setImageBitmap(bm);
			else
				iv.setImageResource(R.drawable.logo_default_channel);

			if (_isLargeScreen) {
				if (_channelsList.getSelectedItemPosition() == position) {
					view.findViewById(R.id.iv_selection_overlay).setVisibility(View.GONE);
					view.findViewById(R.id.ll_channel_item).setBackgroundResource(R.drawable.shape_channel_border_selected);
				} else {
					view.findViewById(R.id.iv_selection_overlay).setVisibility(View.VISIBLE);
					view.findViewById(R.id.ll_channel_item).setBackgroundResource(R.drawable.shape_white_border);
				}
			}

			final TextView tvTitle = ((TextView) view.findViewById(R.id.tv_channel_title));
			String userTitle = cursor.getString(cursor.getColumnIndex(Channels.USER_TITLE));
			if (userTitle.equals("")) {
				tvTitle.setText(cursor.getString(cursor.getColumnIndex(Channels.TITLE)));
				(view.findViewById(R.id.tv_channel_title)).setContentDescription("");

			} else {
				tvTitle.setText(userTitle);
				(view.findViewById(R.id.tv_channel_title)).setContentDescription("");
			}

			final TextView tvSummary = ((TextView) view.findViewById(R.id.tv_channel_summary));
			tvSummary.setText(cursor.getString(cursor.getColumnIndex(Channels.SUMMARY)));
			tvSummary.setContentDescription("");

			view.setContentDescription(Utils.formatString(getString(string.access_item_channel_title, cursor.getString(cursor.getColumnIndex(Channels.TITLE))))+"\n"
					+ Utils.formatString(getString(string.access_item_channel_description, cursor.getString(cursor.getColumnIndex(Channels.SUMMARY)))));

			final ImageView icMore = (ImageView) view.findViewById(R.id.iv_channel_more);
			final Channel channel = new Channel(getActivity().getApplicationContext());
			channel.initDataFromCursor(cursor);
			icMore.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					((Activity_Main) getActivity()).showChannelPopupMenu(channel, v);
				}
			});

			return view;
		}

		@Override
		public void bindView(View arg0, Context arg1, Cursor arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public View newView(Context arg0, Cursor arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			return null;
		}
	}

	public HashMap<String, String> _downloadedImage = new HashMap<String, String>();

	public Bitmap getDownloadedImage(String imageLocalUri) {
		Bitmap bm;
		bm = BitmapFactory.decodeFile(imageLocalUri);
		return bm;
	}

	public Bitmap getImageForChannel(Cursor cursor) {

		String channelThumLocalUri = cursor.getString(cursor.getColumnIndex(Channels.THUMB_LOCAL_URI));
		String channelThumbUrl = cursor.getString(cursor.getColumnIndex(Channels.THUMB_URL));

		Bitmap bm = null;
		if (!channelThumLocalUri.equals("")) {
			bm = getDownloadedImage(channelThumLocalUri);
			_downloadedImage.put(channelThumbUrl, channelThumLocalUri);
		} else {
			if (!channelThumbUrl.equals("")) {
				if (_downloadedImage.containsKey(channelThumbUrl)) {
					if (!_downloadedImage.get(channelThumbUrl).equals("")) {
						bm = getDownloadedImage(_downloadedImage.get(channelThumbUrl));
						Channel channel = new Channel(getActivity().getApplicationContext());
						channel.initDataFromCursor(cursor);
						channel._thumbLocalUri = _downloadedImage.get(channelThumbUrl);
						channel.updateChannel(_context.getContentResolver());
					} else {
					}
				} else {
					_downloadedImage.put(channelThumbUrl, "");
					((Activity_Main) getActivity()).downloadImageForChannel(cursor.getInt(cursor.getColumnIndex(Channels.CHANNEL_ID)));
				}
			}
		}
		return bm;
	}

}
