package com.soreha.scenari_reader.view;


import android.content.Context;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeProvider;
import android.widget.FrameLayout;

import com.soreha.scenari_reader.R.id;

public class ItemChannel extends FrameLayout {


	public ItemChannel(Context context) {
		super(context);
		initView();
		
		ViewCompat.setAccessibilityDelegate(getRootView(), new AccessibilityDelegateCompat(){

			@Override
			public void onInitializeAccessibilityEvent(View host, AccessibilityEvent event) {
				super.onInitializeAccessibilityEvent(host, event);
		        event.getText().add("test");
			}

			@Override
			public void onInitializeAccessibilityNodeInfo(View host, AccessibilityNodeInfoCompat info) {
				super.onInitializeAccessibilityNodeInfo(host, info);
				info.setText("test");
			}

			@Override
			public void onPopulateAccessibilityEvent(View host, AccessibilityEvent event) {
				super.onPopulateAccessibilityEvent(host, event);
			}
		});
		
		
	}


	@Override
	public AccessibilityNodeProvider getAccessibilityNodeProvider() {
		// TODO Auto-generated method stub
		return super.getAccessibilityNodeProvider();
	}

	private void initView() {
		View view = LayoutInflater.from(getContext()).inflate(com.soreha.scenari_reader.R.layout.item_sv_channel, null);
		view.findViewById(id.tv_channel_title).setFocusable(false);
		view.findViewById(id.tv_channel_summary).setFocusable(false);
		addView(view);
	}
}
