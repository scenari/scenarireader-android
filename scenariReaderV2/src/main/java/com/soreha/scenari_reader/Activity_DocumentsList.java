package com.soreha.scenari_reader;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class Activity_DocumentsList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__documents_list);
        Fragment_DocumentsList fragment = Fragment_DocumentsList.newInstance(getIntent().getIntExtra(Fragment_DocumentsList.ARG_CHANNEL_ID,0), getIntent().getStringExtra(Fragment_DocumentsList.ARG_CHANNEL_NAME));
        getFragmentManager().beginTransaction().add(R.id.container,fragment).commit();
    }
}
