package com.soreha.scenari_reader;

import com.soreha.scenari_reader.lib.data.Document;

public interface Interface_DocumentActionListener {
	 void actionOpenDocument(Document document, boolean fromLastPage);
}
