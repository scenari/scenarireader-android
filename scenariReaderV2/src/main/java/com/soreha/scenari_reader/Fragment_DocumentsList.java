package com.soreha.scenari_reader;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.soreha.scenari_reader.R.drawable;
import com.soreha.scenari_reader.R.string;
import com.soreha.scenari_reader.lib.data.Document;
import com.soreha.scenari_reader.lib.data.Document.Documents;
import com.soreha.scenari_reader.lib.util.MyLogs;
import com.soreha.scenari_reader.lib.util.Utils;

import java.util.HashMap;

public class Fragment_DocumentsList extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
	public static final int LOADER_DOCS_CHANNEL = -4444;

	public static final int DOC_FILTER_ALL = 1;
	public static final int DOC_FILTER_DOWNLOADED = 2;
	public static final int DOC_FILTER_FAV = 3;
	public static final int DOC_FILTER_AVAILABLE = 4;
	public static final int DOC_ORDER_RSS = 1;
	public static final int DOC_ORDER_TITLE = 2;
	public static final int DOC_ORDER_DATE = 3;

	public static final String ARG_CHANNEL_ID = "arg_channel_id";
	public static final String ARG_CHANNEL_NAME = "arg_channel_name";

	public int _docFilter = DOC_FILTER_ALL;
	public int _docOrder = DOC_ORDER_RSS;

	public DocumentsCursorAdapter _documentCursorAdapter;
	public AdapterView<BaseAdapter> _documentList;
	public boolean _isLargeScreen = false;
	public String _currentChannelName = "";
	public int _currentChannelId;

	public Context _context;
	public HashMap<String, Bitmap> _imageCache = new HashMap<String, Bitmap>();
	public HashMap<String, String> _downloadedImage = new HashMap<String, String>();
	public Interface_DocumentActionListener _documentActionListener;

	public static Fragment_DocumentsList newInstance(int channelId, String channelName) {

		Bundle args = new Bundle();
		args.putString(ARG_CHANNEL_NAME, channelName);
		args.putInt(ARG_CHANNEL_ID, channelId);

		Fragment_DocumentsList fragment = new Fragment_DocumentsList();
		fragment.setArguments(args);
		return fragment;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		_isLargeScreen = getResources().getBoolean(R.bool.isLargeScreen);
		if (getArguments().containsKey(ARG_CHANNEL_ID)) {
			_currentChannelId = getArguments().getInt(ARG_CHANNEL_ID);
			_currentChannelName = getArguments().getString(ARG_CHANNEL_NAME);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		MyLogs.write("Fragment View Created", MyLogs.TITLE);
		return inflater.inflate(R.layout.fragment_documents_list, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		final String[] cols = new String[] { Documents.TITLE, Documents.SUMMARY, Documents.THUMB_LARGE_LOCAL_URI };
		final int[] views = new int[] { R.id.tv_document_title, R.id.tv_document_summary };

		_documentCursorAdapter = new DocumentsCursorAdapter(getActivity().getApplicationContext(), R.layout.item_sv_document, null, cols, views, 0);
		_documentList = (AdapterView<BaseAdapter>) getActivity().findViewById(R.id.gv_documents);

		_documentList.setAdapter(_documentCursorAdapter);
		_documentList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Cursor cursor = _documentCursorAdapter.getCursor();
				cursor.moveToPosition(arg2);
				Document document = new Document(cursor);
				((Activity_Main) getActivity()).checkStatusBeforeOpeningDocument(document, true);
			}
		});
		_documentList.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Cursor cursor = _documentCursorAdapter.getCursor();
				cursor.moveToPosition(arg2);
				Document document = new Document(cursor);
				((Activity_Main) getActivity()).showDocumentPopupMenu(document, arg1);
				//showDocumentPopupMenu(arg1, document);
				return true;
			}
		});

		if (_currentChannelId != -1)
			initDocumentList(_currentChannelId, _currentChannelName);
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		_context = activity.getApplicationContext();
	}

	public void setDocumentActionListener(Interface_DocumentActionListener documentActionListener) {
		_documentActionListener = documentActionListener;
	}

	public void initDocumentList(int channelId, String channelName) {
		_currentChannelId = channelId;
		_currentChannelName = channelName;
		getLoaderManager().initLoader(LOADER_DOCS_CHANNEL, null, this);
	}

	public void setDocumentsCursor(Cursor cursor) {
		MyLogs.t("Cursor Updated : " + cursor.getCount());
		_documentCursorAdapter.swapCursor(cursor);
		if (!_isLargeScreen) {
			TextView tv = ((TextView) getActivity().findViewById(R.id.tv_doc_list_channel_title));
			tv.setText(_currentChannelName);
			tv.setContentDescription(Utils.formatString(getString(string.access_channel_title),_currentChannelName));
			((Activity_Main) getActivity()).reloadMenu();
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		Uri baseUri;
		baseUri = Documents.CONTENT_URI;
		String query = Documents.CHANNEL_ID + " = " + _currentChannelId;

		switch (_docFilter) {
			case DOC_FILTER_DOWNLOADED:
				query += " AND " + Documents.STATUS + " = " + Document.STATUS_AVAILABLE;
				break;
			case DOC_FILTER_FAV:
				query += " AND " + Documents.IS_FAVORITE + " = 1";
				break;
		}

		String order = "";
		switch (_docOrder) {
			case DOC_ORDER_RSS:
				order = Documents.RSS_POS;
				break;
			case DOC_ORDER_TITLE:
				order = Documents.TITLE;
				break;
			case DOC_ORDER_DATE:
				order = Documents.PUBLICATION_DATE;
				break;
		}

		return new CursorLoader(_context, baseUri, null, query, null, order + " ASC");
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		setDocumentsCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {

	}

	public class DocumentsCursorAdapter extends SimpleCursorAdapter {
		public DocumentsCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
			super(context, layout, c, from, to, flags);
		}

		@Override
		public void bindView(final View view, Context context, final Cursor cursor) {
			super.bindView(view, context, cursor);

			final ImageView iv = (ImageView) view.findViewById(R.id.iv_document_thumb);

			Bitmap bm = Fragment_DocumentsList.this.getImageForDocument(cursor, _isLargeScreen);

			if (bm != null) {
				iv.setImageBitmap(bm);
			} else
				iv.setImageResource(R.drawable.logo_default_document);

			final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.pb_document);
			progressBar.setProgress(cursor.getInt(cursor.getColumnIndex(Documents.STATUS_PROGRESS)));

			final TextView tv = (TextView) view.findViewById(R.id.tv_document_status);

			int status = cursor.getInt(cursor.getColumnIndex(Documents.STATUS));
			String statusText;
			int statusTextColor;
			switch (status) {
			case Document.STATUS_NOT_DOWNLOADED:
				if (cursor.getString(cursor.getColumnIndex(Documents.ONLINE_URL)).endsWith("html")) {
					statusText = getString(R.string.doc_status_notDownloaded_online);
				} else {
					statusText = getString(R.string.doc_status_not_downloaded);
				}
				statusTextColor = 0xffff5f5f;
				progressBar.setVisibility(View.GONE);
				break;
			case Document.STATUS_DOWNLOADING:
				statusText = getString(R.string.doc_status_downloading);
				statusTextColor = 0xffffbb33;
				progressBar.setVisibility(View.VISIBLE);
				break;
			case Document.STATUS_WAITING_FOR_DOWNLOAD:
				statusText = getString(R.string.doc_status_waiting);
				statusTextColor = 0xffffbb33;
				progressBar.setVisibility(View.GONE);
				break;
			case Document.STATUS_DOWNLOAD_PAUSED:
				statusText = getString(R.string.doc_status_paused);
				statusTextColor = 0xffffbb33;
				progressBar.setVisibility(View.GONE);
				break;
			case Document.STATUS_DOWNLOAD_INTERRUPTED:
				statusText = getString(R.string.doc_status_downloading);
				statusTextColor = 0xffffbb33;
				progressBar.setVisibility(View.GONE);
				break;
			case Document.STATUS_EXTRACTING:
				statusText = getString(R.string.doc_status_extracting);
				statusTextColor = 0xffffbb33;
				progressBar.setVisibility(View.VISIBLE);
				break;
			default:
				statusText = getString(R.string.doc_status_downloaded);
				statusTextColor = 0xff99cc00;
				progressBar.setVisibility(View.GONE);
				break;
			}

			tv.setText(statusText);
			tv.setTextColor(statusTextColor);
			tv.setContentDescription(Utils.formatString(getString(string.access_item_doc_status), statusText).replace("/", " "));

			((TextView) view.findViewById(R.id.tv_document_title)).setContentDescription(Utils.formatString(getString(string.access_item_doc_title),
					cursor.getString(cursor.getColumnIndex(Documents.TITLE))));
			((TextView) view.findViewById(R.id.tv_document_summary)).setContentDescription(Utils.formatString(getString(string.access_item_doc_description),
					cursor.getString(cursor.getColumnIndex(Documents.SUMMARY))));

			final ImageView ivFav = (ImageView) view.findViewById(R.id.iv_document_fav);
			if (cursor.getInt(cursor.getColumnIndex(Documents.IS_FAVORITE)) == 1)
				ivFav.setVisibility(View.VISIBLE);
			else
				ivFav.setVisibility(View.GONE);

			final ImageView ivDocType = (ImageView) view.findViewById(R.id.iv_document_type);
			String docType = cursor.getString(cursor.getColumnIndex(Documents.TYPE));
			if (docType.equals(Document.DOCUMENT_TYPE_OPALE)) {
				ivDocType.setImageResource(drawable.ic_type_opale_small);
				ivDocType.setVisibility(View.VISIBLE);
				ivDocType.setContentDescription(Utils.formatString(getString(string.access_item_doc_type), cursor.getString(cursor.getColumnIndex(Documents.TYPE))));
			} else if (docType.equals(Document.DOCUMENT_TYPE_TOPAZE)){
				ivDocType.setImageResource(drawable.ic_type_topaze_small);
				ivDocType.setVisibility(View.VISIBLE);
				ivDocType.setContentDescription(Utils.formatString(getString(string.access_item_doc_type), cursor.getString(cursor.getColumnIndex(Documents.TYPE))));
			}
			else 
				ivDocType.setVisibility(View.GONE);

			final Document document = new Document(cursor);
			final ImageView icMore = (ImageView) view.findViewById(R.id.iv_document_more);
			icMore.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					
					((Activity_Main) getActivity()).showDocumentPopupMenu(document, v);
				}
			});
		}
	}

	public Bitmap getDownloadedImage(String imageLocalUri) {
		Bitmap bm;
		if (_imageCache.containsKey(imageLocalUri)) {
			bm = _imageCache.get(imageLocalUri);
		} else {
			bm = BitmapFactory.decodeFile(imageLocalUri);
			_imageCache.put(imageLocalUri, bm);
		}

		return bm;
	}

	public Bitmap getImageForDocument(Cursor cursor, boolean isLargeImage) {

		String localUri;
		String url;

		if (isLargeImage) {
			localUri = cursor.getString(cursor.getColumnIndex(Documents.THUMB_LARGE_LOCAL_URI));
			url = cursor.getString(cursor.getColumnIndex(Documents.THUMB_LARGE_URL));
		} else {
			localUri = cursor.getString(cursor.getColumnIndex(Documents.THUMB_SMALL_LOCAL_URI));
			url = cursor.getString(cursor.getColumnIndex(Documents.THUMB_SMALL_URL));
		}

		Bitmap bm = null;
		if (!localUri.equals("")) {
			bm = getDownloadedImage(localUri);
			_downloadedImage.put(url, localUri);
		} else {
			if (!url.equals("")) {
				if (_downloadedImage.containsKey(url)) {
					if (!_downloadedImage.get(url).equals("")) {
						bm = getDownloadedImage(_downloadedImage.get(url));
						Document document = new Document(getActivity().getApplicationContext());
						document.initDataWithCursor(cursor);
						if (isLargeImage)
							document._largeThumbLocalUri = _downloadedImage.get(url);
						else
							document._smallThumbLocalUri = _downloadedImage.get(url);
						document.updateDocument(_context.getContentResolver());
					} else {
					}
				} else {
					_downloadedImage.put(url, "");
					((Activity_Main) getActivity()).downloadImageForDocument(cursor.getInt(cursor.getColumnIndex(Documents.DOCUMENT_ID)),
							isLargeImage);

				}
			}
		}
		return bm;
	}

	public void onClickDocument(Document document) {

	}

	public void onLongClickDocument(Document document) {

	}

	public void onRadioButtonClicked(View view) {
		switch (view.getId()) {
			case R.id.radio_doc_all:
				_docFilter = DOC_FILTER_ALL;
				break;
			case R.id.radio_doc_downloaded:
				_docFilter = DOC_FILTER_DOWNLOADED;
				break;
			case R.id.radio_doc_fav:
				_docFilter = DOC_FILTER_FAV;
				break;
			case R.id.radio_order_title:
				_docOrder = DOC_ORDER_TITLE;
				break;
			case R.id.radio_order_pub:
				_docOrder = DOC_ORDER_RSS;
				break;
			case R.id.radio_order_date:
				_docOrder = DOC_ORDER_DATE;
				break;
		}

		getLoaderManager().restartLoader(LOADER_DOCS_CHANNEL, null, this);
	}
}
