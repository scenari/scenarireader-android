package com.soreha.scenari_reader;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.soreha.scenari_reader.R.id;
import com.soreha.scenari_reader.dialogs.DialogFragment_AddUpdateChannel;
import com.soreha.scenari_reader.dialogs.DialogFragment_ChannelInfos;
import com.soreha.scenari_reader.dialogs.DialogFragment_DocumentInfos;
import com.soreha.scenari_reader.dialogs.DialogFragment_DownloadManager;
import com.soreha.scenari_reader.dialogs.DialogFragment_ListBookmarks;
import com.soreha.scenari_reader.dialogs.DialogFragment_SearchDoc;
import com.soreha.scenari_reader.lib.comm.RequestManager;
import com.soreha.scenari_reader.lib.data.Channel;
import com.soreha.scenari_reader.lib.data.Document;
import com.soreha.scenari_reader.lib.data.Document.Documents;
import com.soreha.scenari_reader.lib.service.DownloadService;
import com.soreha.scenari_reader.lib.service.IRemoteBackgroundService;
import com.soreha.scenari_reader.lib.service.IRemoteBackgroundServiceCallback;
import com.soreha.scenari_reader.lib.util.MyLogs;
import com.soreha.scenari_reader.lib.util.Utils;

import java.io.File;
import java.util.Formatter;

public class Activity_Main extends AppCompatActivity implements /*LoaderManager.LoaderCallbacks<Cursor>,*/ FragmentManager.OnBackStackChangedListener, Interface_DocumentActionListener, Fragment_ChannelsList.ChannelListener {
	private static final int REQUEST_ADD_CHANNEL = 11;
	private static final int REQUEST_OPEN_DOC = 22;


	/*public static final int LOADER_AVAILABLE_DOCS_ID = -6666;
	public static final int LOADER_DOWNLOAD_MANAGER_ID = -7777;
	public static final int LOADER_SEARCH_DOC_ID = -8888;*/


	public Fragment_ChannelsList _fragementChannelsList;
	public Fragment_DocumentsList _fragmentDocumentsList;

	public boolean _isLargeScreen = false;

	public ActionBar _actionBar;
	public FragmentManager _fragmentManager;

	public RequestManager _requestManager;

	public SharedPreferences _sharedPreferences;

	public int _currentChannelId = -1;
	public String _currentChannelName = "";
	public boolean _updatesChecked = false;


	public DialogFragment_DownloadManager _dialogDownloadManager = null;

	public boolean isDownloadManagerActionItemVisible = true;

	public IRemoteBackgroundService _serviceDocDownloader;

	/************************************ Activity LifeCycle *****************************/

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

		setContentView(R.layout.activity_main);

		setProgressBarIndeterminateVisibility(false);

		_actionBar = getSupportActionBar();
		_actionBar.setSubtitle("Version "+getString(R.string.version));

		File storageDirectoryPath = new File(getExternalFilesDir(null).getAbsolutePath() + "/docs");
		if (!storageDirectoryPath.exists()) {
			MyLogs.write("CREATING STORAGE PATH", MyLogs.TITLE);
			new File(getExternalFilesDir(null).getAbsolutePath() + "/docs").mkdirs();
			new File(getExternalFilesDir(null).getAbsolutePath() + "/tmp").mkdirs();
			new File(getExternalFilesDir(null).getAbsolutePath() + "/thumb").mkdirs();
		}

		_fragmentManager = getFragmentManager();
		_fragmentManager.addOnBackStackChangedListener(this);
		initFragment(savedInstanceState);

		_requestManager = new RequestManager(getApplicationContext(), _requestHandler);

		_sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		if (_sharedPreferences.getBoolean("firstRun", true)) {
			addDefaultChannels();
		}


		checkAction(getIntent());
	}

	@Override
	protected void onNewIntent(Intent intent) {
		MyLogs.write("ON NEW INTENT", MyLogs.TITLE);
		checkAction(intent);
		super.onNewIntent(intent);
	}

	/*
	 * Ouverture du download Manager suite au clic sur une notification de
	 * téléchargement
	 */
	private void checkAction(Intent intent) {
		Bundle b = intent.getExtras();
		if (b != null) {
			int action = b.getInt(DownloadService.NOTIFICATION_ACTION, -1);
			if (action == DownloadService.ACTION_OPEN_DOWNLOAD_MANAGER)
				openDownloadManager();
			// else if (action == ACTION_OPEN_DOCUMENT)
			// startReaderActivity((Document)b.getParcelable(DOCUMENT),false);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		try {
			if (_serviceDocDownloader != null)
				_serviceDocDownloader.unregisterCallback(callback);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		unbindService(remoteConnection);
		super.onPause();
	}

	@Override
	protected void onResume() {
		Intent serviceIntent = new Intent(getApplicationContext(), DownloadService.class);
		serviceIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startService(serviceIntent);
		bindService(serviceIntent, remoteConnection, Context.BIND_AUTO_CREATE);
		super.onResume();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	public void onBackStackChanged() {

	}

	public void initFragment(Bundle savedInstanceState) {

		if (savedInstanceState == null) {
			_isLargeScreen = getResources().getBoolean(R.bool.isLargeScreen);
			_fragementChannelsList = Fragment_ChannelsList.newInstance();
			_fragmentManager.beginTransaction().add(id.fragment_container_channel_list,_fragementChannelsList).commit();

			if (findViewById(id.fragment_container_document_list) != null) {
				_fragmentDocumentsList = Fragment_DocumentsList.newInstance(-1,"");
				_fragmentManager.beginTransaction().add(id.fragment_container_document_list,_fragmentDocumentsList).commit();
				_fragmentDocumentsList.setDocumentActionListener(this);
			}
		}
	}

	public void addDefaultChannels() {
		int id;

		Channel channel = new Channel(getApplicationContext());
		channel._rssUrl = "http://scenari-platform.org/mobile-source/opale-demo/index.rss";
		channel._status = Channel.STATUS_VALIDATED;
		if ((id = Channel.isChannelInDBForURL(getContentResolver(), channel._rssUrl)) == -1) {
			channel.insertChannel(getContentResolver());
			_requestManager.addRssRequest(channel);
			MyLogs.d("Channel inserted with id : " + channel._id);
		} else {
			channel._id = id;
			MyLogs.d("Existing Channel with id : " + id);
		}

		SharedPreferences.Editor editor = _sharedPreferences.edit();
		editor.putBoolean("firstRun", false);
		editor.commit();
	}

	/*********************** Loader Management ****************************/
	public void initDocumentsLoaderForChannelId(int channelId, String channelName) {
		_currentChannelId = channelId;
		_currentChannelName = channelName;
		_fragmentDocumentsList.initDocumentList(_currentChannelId, _currentChannelName);
	}



	/************************* Document Actions *****************************/

	@Override
	public void actionOpenDocument(Document document, boolean fromLastPage) {
		checkStatusBeforeOpeningDocument(document, fromLastPage);
	}

	@Override
	public void checkRssForChannel(Channel channel) {
		_requestManager.addRssRequest(channel);
	}



	/************************* Dialogs ***************************/


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_ADD_CHANNEL :
                    _requestManager.addRssRequest((Channel) data.getParcelableExtra("channel"));
                    break;
                case REQUEST_OPEN_DOC :
                    checkStatusBeforeOpeningDocument((Document) data.getParcelableExtra("document"),false);
                    break;
            }
		}
	}

	public void openAddChannelDialog(Channel channel) {
		Intent intent = new Intent(this,DialogFragment_AddUpdateChannel.class);
		startActivityForResult(intent, REQUEST_ADD_CHANNEL);
	}

	public void openDownloadManager() {
		Intent intent = new Intent(this,DialogFragment_DownloadManager.class);
		startActivity(intent);
	}

	public void openSearchDialog() {
        Intent intent = new Intent(this,DialogFragment_SearchDoc.class);
        startActivityForResult(intent,REQUEST_OPEN_DOC);
	}

    public void showBookmarkListDialog() {
        Intent intent = new Intent(this,DialogFragment_ListBookmarks.class);
        startActivityForResult(intent,REQUEST_OPEN_DOC);
    }

	public void showDocumentInfosDialog(Document document) {
		Intent intent = new Intent(this,DialogFragment_DocumentInfos.class);
		intent.putExtra("document",document);
		startActivity(intent);
	}

	public void showChannelInfosDialog(Channel channel) {
		Intent intent = new Intent(this,DialogFragment_ChannelInfos.class);
		intent.putExtra("channel",channel);
		startActivity(intent);
	}

	public void showDocumentPopupMenu(final Document document, View view) {

		String favLabel = getString(R.string.menu_doc_fav_add);
		if (document._isFavorite)
			favLabel = getString(R.string.menu_doc_fav_delete);

		PopupMenu popupMenu = new PopupMenu(this, view);
		
		if (document._status == Document.STATUS_AVAILABLE){
			popupMenu.getMenu().add(Menu.NONE, 0, Menu.NONE, R.string.menu_doc_open);
			popupMenu.getMenu().add(Menu.NONE, 1, Menu.NONE, R.string.menu_doc_open_lastpage);
			popupMenu.getMenu().add(Menu.NONE, 2, Menu.NONE, favLabel);
			popupMenu.getMenu().add(Menu.NONE, 3, Menu.NONE, R.string.menu_doc_infos);
			popupMenu.getMenu().add(Menu.NONE, 4, Menu.NONE, R.string.menu_doc_delete);
		}else {
			popupMenu.getMenu().add(Menu.NONE, 5, Menu.NONE, R.string.menu_doc_open_online);
			popupMenu.getMenu().add(Menu.NONE, 6, Menu.NONE, R.string.menu_doc_open_lastpage_online);
			popupMenu.getMenu().add(Menu.NONE, 2, Menu.NONE, favLabel);
			popupMenu.getMenu().add(Menu.NONE, 3, Menu.NONE, R.string.menu_doc_infos);
			popupMenu.getMenu().add(Menu.NONE, 7, Menu.NONE, R.string.menu_doc_download);
		}
		
		if (document._status != Document.STATUS_AVAILABLE && !document._onlineUrl.endsWith("html")){
			popupMenu.getMenu().getItem(0).setEnabled(false);
			popupMenu.getMenu().getItem(1).setEnabled(false);
			popupMenu.getMenu().getItem(2).setEnabled(false);
		}
		
		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem arg0) {
				switch (arg0.getItemId()) {
				case 0:
					startReaderActivity(document, false, true);
					break;
				case 1:
					startReaderActivity(document, true, true);
					break;
				case 2:
					document.addRemoveFav(getContentResolver());
					break;
				case 3:
					showDocumentInfosDialog(document);
					break;
				case 4:
					new Thread(new Runnable() {
						@Override
						public void run() {
							document.removeFromShelves(getContentResolver());
						}
					}).start();
					break;
				case 5:
					startReaderActivity(document, false, false);
					break;
				case 6:
					startReaderActivity(document, true, false);
					break;
				case 7:
					try {
						_serviceDocDownloader.addNewDocumentRequest(document);
					} catch (RemoteException e) {
						e.printStackTrace();
					}
					break;
				}
				return false;
			}
		});
		popupMenu.show();
		
		popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
			
			@Override
			public void onDismiss(PopupMenu arg0) {
				//arg0.
//				if (!toastText.equals(""))
//					Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_LONG).show();
			}
		});

	}
	
	public void showChannelPopupMenu (final Channel channel, View view){
		_fragementChannelsList._isChannelMenuOpened = true;
		PopupMenu popupMenu = new PopupMenu(this, view);

		popupMenu.getMenu().add(Menu.NONE, 0, Menu.NONE, R.string.menu_doc_infos);
		popupMenu.getMenu().add(Menu.NONE, 1, Menu.NONE, R.string.menu_channel_modify);
		popupMenu.getMenu().add(Menu.NONE, 2, Menu.NONE, R.string.menu_doc_delete);
		
		
		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem arg0) {
				switch (arg0.getItemId()) {
				case 0:
					showChannelInfosDialog(channel);
					break;
				case 1:
					openAddChannelDialog(channel);
					break;
				case 2:
					removeChannel(channel);
					break;
				}
				_fragementChannelsList._isChannelMenuOpened = false;
				return false;
			}
		});
		popupMenu.show();
	}

	

	public void showDialogDocumentNotDownloaded(final Document document) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(R.string.alert_dialog_title_infos);
		builder.setMessage(R.string.alert_dialog_message_doc_notdownloaded);

		builder.setPositiveButton(R.string.alert_dialog_oui, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				try {
					_serviceDocDownloader.addNewDocumentRequest(document);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		});

		builder.setNegativeButton(R.string.alert_dialog_non, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		builder.show();
	}

	public void showDialogDocumentNotDownloadedAvailableOnline(final Document document) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(R.string.alert_dialog_title_infos);
		builder.setMessage(R.string.alert_dialog_message_doc_notdownloaded_online);

		builder.setPositiveButton(R.string.menu_doc_download, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				try {
					_serviceDocDownloader.addNewDocumentRequest(document);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		});

		builder.setNeutralButton(R.string.alert_dialog_open_online, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				startReaderActivity(document, false, false);
			}
		});

		builder.setNegativeButton(R.string.alert_dialog_close, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		builder.show();
	}

	public void showDialogUpdateAvailable(final Document document, final boolean fromLastPage, final boolean forOpening) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.alert_dialog_title_infos);
		builder.setMessage(R.string.alert_dialog_update_available);

		builder.setPositiveButton(R.string.alert_dialog_oui, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
				openDownloadManager();
			}
		});

		builder.setNegativeButton(R.string.alert_dialog_non, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				//if (forOpening)
					startReaderActivity(document, fromLastPage, true);
				//else
				//	showDocumentActionDialog(document);
				dialog.cancel();
			}
		});
		builder.show();
	}



	public void checkStatusBeforeOpeningDocument(final Document document, final boolean fromLastPage) {
		MyLogs.d("Status : " + document._status);

		switch (document._status) {
		case Document.STATUS_NOT_DOWNLOADED:
			if (!document._onlineUrl.endsWith("html"))
				showDialogDocumentNotDownloaded(document);
			else
				showDialogDocumentNotDownloadedAvailableOnline(document);
			break;
		case Document.STATUS_AVAILABLE:
			if (document._updateAvailable == false)
				startReaderActivity(document, fromLastPage, true);
			else {
				showDialogUpdateAvailable(document, fromLastPage, true);
			}
			break;
		default:
			Utils.showStandardAlertDialog(this, getString(R.string.alert_dialog_title_infos), getString(R.string.alert_dialog_message_downloading));
		}
	}

	@SuppressLint("HandlerLeak")
	public final Handler _requestHandler = new Handler() {
		@Override
		public void handleMessage(final Message msg) {
			final Channel channel = msg.getData().getParcelable("channel");
			int status = 0;
			if (channel != null)
				status = channel._status;

			AlertDialog.Builder builder = new AlertDialog.Builder(Activity_Main.this);

			switch (msg.what) {
			case Channel.URL_ERROR:
				MyLogs.write("Erreur url", MyLogs.ERROR);

				if (status == Channel.STATUS_NOT_VALIDATED) {
					builder.setTitle(R.string.alert_dialog_title_error);
					builder.setMessage(R.string.alert_dialog_message_doc_notfound);

					builder.setNegativeButton(R.string.alert_dialog_close, new OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which) {
							removeChannel(channel);
							dialog.dismiss();
						}
					});

					builder.show();
				}
				break;
			case Channel.NETWORK_ERROR:
				if (status == Channel.STATUS_NOT_VALIDATED) {
					builder.setTitle(R.string.alert_dialog_title_error);
					StringBuilder message = new StringBuilder();
					Formatter formatter = new Formatter(message);
					formatter.format(getString(R.string.alert_dialog_message_connexion_error), channel._rssUrl);
					builder.setMessage(message.toString());
					builder.setNegativeButton(R.string.alert_dialog_close, new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
					builder.show();
				}
				MyLogs.write("Erreur r�seau", MyLogs.ERROR);
				break;
			case Channel.RSS_ERROR:
				if (status == Channel.STATUS_NOT_VALIDATED) {
					builder.setTitle(R.string.alert_dialog_title_error);
					builder.setMessage(R.string.alert_dialog_message_rss_error);
					builder.setNegativeButton(R.string.alert_dialog_close, new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							removeChannel(channel);
							dialog.dismiss();
						}
					});
					builder.show();
				}
				MyLogs.write("Erreur flux rss", MyLogs.ERROR);
				break;
			case Channel.EMPTY_CHANNEL:
				MyLogs.write("Chaine vide", MyLogs.ERROR);
				if (status == Channel.STATUS_NOT_VALIDATED) {
					builder.setTitle(R.string.alert_dialog_title_error);
					builder.setMessage(R.string.alert_dialog_message_emptychannel);
					builder.setPositiveButton(R.string.alert_dialog_oui, new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
					builder.setNegativeButton(R.string.alert_dialog_non, new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							removeChannel(channel);
							dialog.dismiss();
						}
					});
					builder.show();
				}
				break;
			case RequestManager.ALL_REQUESTS_DONE:
				hidProgressBar();
				if (!_updatesChecked) {
					String query = Documents.UPDATE_AVAILABLE + " = 1 AND " + Documents.STATUS + " = " + Document.STATUS_AVAILABLE;
					Cursor c = getContentResolver().query(Documents.CONTENT_URI, null, query, null, Documents.DOCUMENT_ID + " ASC");
					if (c.getCount() > 0) {
						while (c.moveToNext()) {
							try {
								_serviceDocDownloader.addNewDocumentRequest(new Document(c));
							} catch (RemoteException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						c.close();

						builder.setTitle(R.string.alert_dialog_title_update);
						builder.setMessage(R.string.alert_dialog_message_updates_available);
						builder.setPositiveButton(R.string.alert_dialog_oui, new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								openDownloadManager();
								dialog.dismiss();
							}
						});
						builder.setNegativeButton(R.string.alert_dialog_non, new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
						builder.show();
					}

					_updatesChecked = true;
				}
				break;
			default:
				break;
			}
		}
	};

	public void showProgressBar() {
		setProgressBarIndeterminateVisibility(true);
	}

	public void hidProgressBar() {
		setProgressBarIndeterminateVisibility(false);
	}

	// public abstract void updateDocumentProgressBar(int progress, String tag);

	final IRemoteBackgroundServiceCallback callback = new IRemoteBackgroundServiceCallback.Stub() {
		@Override
		public void dataChanged(int progress, String tag) throws RemoteException {
			// updateDocumentProgressBar(progress, "Progress" + tag);

		}

		@Override
		public void showActionBarButton() throws RemoteException {
			// isDownloadManagerActionItemVisible = true;
			// reloadMenu();
		}

		@Override
		public void hideActionBarButton() throws RemoteException {
			// isDownloadManagerActionItemVisible = false;
			// reloadMenu();
		}
	};

	public void reloadMenu() {
		invalidateOptionsMenu();
	}

	private ServiceConnection remoteConnection = new ServiceConnection() {
		public void onServiceDisconnected(ComponentName name) {
			MyLogs.write("DISCONNECT SERVICE", MyLogs.TITLE);
			try {
				MyLogs.write("REMOVE SERVICE CALLBACK", MyLogs.TITLE);
				_serviceDocDownloader.unregisterCallback(callback);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			_serviceDocDownloader = null;
		}

		public void onServiceConnected(ComponentName name, IBinder service) {
			_serviceDocDownloader = IRemoteBackgroundService.Stub.asInterface(service);
			try {
				_serviceDocDownloader.registerCallback(callback);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	};

	public void downloadImageForChannel(int channelId) {
		_requestManager.addThumbnailRequestForChannel(channelId);
	}

	public void downloadImageForDocument(int documentId, boolean isLargeImage) {
		_requestManager.addThumbnailRequest(documentId, isLargeImage);
	}

	public void startReaderActivity(Document document, boolean fromLastPage, boolean fromLocal) {
		Intent intent = new Intent(getApplicationContext(), Activity_Reader.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra("document", document);
		intent.putExtra("isLarge", _isLargeScreen);
		if (!document._lastOpenedPage.equals(""))
			intent.putExtra("fromLastPage", fromLastPage);
		intent.putExtra("fromLocal", fromLocal);
		startActivity(intent);
	}

	protected void removeChannel(Channel channel) {
		MyLogs.t("Channel Removed");
		channel.delete(getContentResolver());
		_fragementChannelsList.channelDeleted();
	}

	public void onRadioButtonClicked(View view) {
		_fragmentDocumentsList.onRadioButtonClicked(view);
	}

	public void openDocumentsListFragmentForChannelId(int channelId, String channelName) {
		FragmentTransaction plTransaction = _fragmentManager.beginTransaction();
		plTransaction.addToBackStack(null);
		_fragmentDocumentsList = Fragment_DocumentsList.newInstance(channelId,channelName);
		_fragmentDocumentsList.setDocumentActionListener(this);

		plTransaction.replace(R.id.fragment_container_channel_list, _fragmentDocumentsList).commit();
		reloadMenu();
		MyLogs.t("Fragment Document Opened");
	}

	@Override
	public void onBackPressed() {
		if (_isLargeScreen)
			super.onBackPressed();
		else if (!_fragmentManager.popBackStackImmediate()) {
			finish();
		} else {
			reloadMenu();
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MyLogs.t("MENU RELOADED");
		int size = menu.size();
		for (int i = 0; i < size; i++) {
			MenuItem item = menu.getItem(i);
			if (item.getItemId() == R.id.download_manager)
				item.setVisible(isDownloadManagerActionItemVisible);
			if (!_isLargeScreen && item.getItemId() == R.id.search)
				if (_fragmentDocumentsList != null && _fragmentDocumentsList.isVisible()) {
					item.setVisible(true);
				} else {
					item.setVisible(false);
				}
		}
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_main, menu);
		return super.onCreateOptionsMenu(menu);
	}



	/*@Override
	public final boolean onMenuItemSelected(int featureId, MenuItem item) {
		MyLogs.write("ITEM CLICKED", MyLogs.ERROR);
		if (item.getItemId() == R.id.search) {
			openSearchDialog();
		} else if (item.getItemId() == R.id.download_manager) {
			openDownloadManager();
		} else if (item.getItemId() == R.id.fav_list)
			showBookmarkListDialog();
		return true;
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.search) {
			openSearchDialog();
		} else if (item.getItemId() == R.id.download_manager) {
			openDownloadManager();
		} else if (item.getItemId() == R.id.fav_list)
			showBookmarkListDialog();
		return true;
	}
	
	
}