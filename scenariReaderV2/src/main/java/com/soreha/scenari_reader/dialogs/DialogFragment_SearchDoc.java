package com.soreha.scenari_reader.dialogs;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.soreha.scenari_reader.R;
import com.soreha.scenari_reader.lib.comm.RequestManager;
import com.soreha.scenari_reader.lib.data.Document;
import com.soreha.scenari_reader.lib.data.Document.Documents;
import com.soreha.scenari_reader.lib.util.StringOperation;

import java.util.HashMap;

public class DialogFragment_SearchDoc extends AppCompatActivity implements LoaderCallbacks<Cursor> {


	public static final int LOADER_SEARCH_DOC_ID = -8888;


	private Context _context;
	public MySimpleCursorAdapter _listAdapter;
	private ListView _listView;
	private EditText _searchView;
	private LoaderManager _loaderManager;
	private int _currentChannelId;
	
	



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_search_docs);
		_currentChannelId = getIntent().getIntExtra("channelId",0);

		init();
	}

	public void init() {

		setResult(RESULT_CANCELED);
		setTitle(R.string.search_dialog_title);
		_context = getApplicationContext();
		_loaderManager = getSupportLoaderManager();

		
		_listView = (ListView) findViewById(R.id.list_available_docs);
		_listView.setEmptyView(findViewById(R.id.list_available_docs_empty));

		_searchView = (EditText) findViewById(R.id.search_doc_tvsearch);

		_searchView.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				if (_searchView.getText().toString().length() > 2) {
					Bundle b = new Bundle();
					b.putCharSequence("search", _searchView.getText().toString());
					if (_loaderManager.getLoader(LOADER_SEARCH_DOC_ID)== null)
						_loaderManager.initLoader(LOADER_SEARCH_DOC_ID, b, DialogFragment_SearchDoc.this);
					else
						_loaderManager.restartLoader(LOADER_SEARCH_DOC_ID, b, DialogFragment_SearchDoc.this);
				} else {
					_loaderManager.destroyLoader(LOADER_SEARCH_DOC_ID);
				}
			}

			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			}

			public void onTextChanged(CharSequence chars, int start, int before, int count) {
			}
		});

		final String[] cols = new String[] { Documents.TITLE, Documents.AUTHOR, Documents.THUMB_SMALL_LOCAL_URI };
		final int[] views = new int[] { R.id.document_title, R.id.document_subtitle };
		_listAdapter = new MySimpleCursorAdapter(_context, R.layout.item_list_document, null, cols, views, 0);

		_listView.setAdapter(_listAdapter);
		_listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Document document = new Document(_context);
				Cursor cursor = (Cursor) _listAdapter.getItem(arg2);
				document.initDataFromDocumentWithId(_context.getContentResolver(), cursor.getInt(cursor.getColumnIndex(Documents.DOCUMENT_ID)));

				Intent intent = new Intent();
				intent.putExtra("document",document);
				setResult(RESULT_OK, intent);
				finish();
			}
		});
	}
	
	
	
	


	private class MySimpleCursorAdapter extends SimpleCursorAdapter {

		public HashMap<String, Bitmap> _imageCache = new HashMap<String, Bitmap>();
		public HashMap<String, String> _downloadedImage = new HashMap<String, String>();
		RequestManager requestManager = new RequestManager(_context, null);

		public MySimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
			super(context, layout, c, from, to, flags);
		}

		public Bitmap getDownloadedImage(String imageLocalUri) {
			Bitmap bm;
			if (_imageCache.containsKey(imageLocalUri)) {
				bm = _imageCache.get(imageLocalUri);
			} else {
				bm = BitmapFactory.decodeFile(imageLocalUri);
				_imageCache.put(imageLocalUri, bm);
			}
			
			return bm;
		}
		
		public Bitmap getImageForDocument(Cursor cursor){
			String documentSmallThumLocalUri = cursor.getString(cursor.getColumnIndex(Documents.THUMB_SMALL_LOCAL_URI));
			String documentSmallThumbUrl = cursor.getString(cursor.getColumnIndex(Documents.THUMB_SMALL_URL));
			
			Bitmap bm = null;
			if (!documentSmallThumLocalUri.equals("")) {
				bm = getDownloadedImage(documentSmallThumLocalUri);
				_downloadedImage.put(documentSmallThumbUrl, documentSmallThumLocalUri);
			} else {
				if (!documentSmallThumbUrl.equals("")) {
					if (_downloadedImage.containsKey(documentSmallThumbUrl)) {
						if (!_downloadedImage.get(documentSmallThumbUrl).equals("")) {
							bm = getDownloadedImage(_downloadedImage.get(documentSmallThumbUrl));
							Document document = new Document(_context);
							document.initDataWithCursor(cursor);
								document._smallThumbLocalUri = _downloadedImage.get(documentSmallThumbUrl);
							document.updateThumb(_context.getContentResolver());
						} else {
						}
					} else {
						_downloadedImage.put(documentSmallThumbUrl, "");
						requestManager.addThumbnailRequest(cursor.getInt(cursor.getColumnIndex(Documents.DOCUMENT_ID)),false);
					}
				}
			}
			return bm;
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			super.bindView(view, context, cursor);

			final ImageView iv = (ImageView) view.findViewById(R.id.iv_document_smallthumb);
			Bitmap bm = getImageForDocument(cursor);
			if (bm != null){
				iv.setImageBitmap(bm);
			} else {
				iv.setImageResource(R.drawable.ic_default_thumb);
			}

			if (cursor.getInt(cursor.getColumnIndex(Documents.IS_FAVORITE)) == 0) {
				( view.findViewById(R.id.iv_document_fav)).setVisibility(View.GONE);
			} else {
				( view.findViewById(R.id.iv_document_fav)).setVisibility(View.VISIBLE);
			}

			
			FrameLayout fl = (FrameLayout) view.findViewById(R.id.fl_more);
			fl.setVisibility(View.GONE);
			
			int status = cursor.getInt(cursor.getColumnIndex(Documents.STATUS));
			
			final TextView tv = ((TextView) view.findViewById(R.id.document_status));
			tv.setTextColor(0xffffffff);
			
			switch (status) {
			case Document.STATUS_NOT_DOWNLOADED:
				((TextView) view.findViewById(R.id.document_status)).setText(R.string.doc_status_not_downloaded);
				tv.setTextColor(0xffff5f5f);
				break;
			case Document.STATUS_DOWNLOADING:
				((TextView) view.findViewById(R.id.document_status)).setText(R.string.doc_status_downloading);
				tv.setTextColor(0xffffbb33);
				break;
			case Document.STATUS_WAITING_FOR_DOWNLOAD:
				((TextView) view.findViewById(R.id.document_status)).setText(R.string.doc_status_waiting);
				tv.setTextColor(0xffffbb33);
				break;
			case Document.STATUS_DOWNLOAD_PAUSED:
				((TextView) view.findViewById(R.id.document_status)).setText(R.string.doc_status_paused);
				tv.setTextColor(0xffffbb33);
				break;
			case Document.STATUS_DOWNLOAD_INTERRUPTED:
				((TextView) view.findViewById(R.id.document_status)).setText(R.string.doc_status_downloading);
				tv.setTextColor(0xffffbb33);
				break;
			case Document.STATUS_EXTRACTING:
				((TextView) view.findViewById(R.id.document_status)).setText(R.string.doc_status_extracting);
				tv.setTextColor(0xffffbb33);
				break;
			default:
				((TextView) view.findViewById(R.id.document_status)).setText(R.string.doc_status_downloaded);
				tv.setTextColor(0xff99cc00);
				break;
			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			return super.newView(context, cursor, parent);
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		String[] sArgs = new String[] { "%" + StringOperation.sansAccent(args.getString("search")) + "%"};
		return new CursorLoader(_context, Documents.CONTENT_URI, null,  Documents.NORMALIZED_SEARCH_FIELD + " LIKE ?", sArgs, Documents.RSS_POS + " ASC");
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		_listAdapter.swapCursor(arg1);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		_listAdapter.swapCursor(null);
	}
}
