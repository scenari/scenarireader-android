package com.soreha.scenari_reader.dialogs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.soreha.scenari_reader.R;
import com.soreha.scenari_reader.R.string;
import com.soreha.scenari_reader.lib.data.Document;
import com.soreha.scenari_reader.lib.util.MyLogs;
import com.soreha.scenari_reader.lib.util.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DialogFragment_DocumentInfos extends AppCompatActivity {

	private Document _document;
	private final DateFormat _dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.dialog_document_infos);

		_document = getIntent().getParcelableExtra("document");

		initViews();
	}



	public void initViews() {
		MyLogs.write("Fragment View Created", MyLogs.TITLE);
		setTitle(string.doc_info_dialog_title);
		
		((TextView)findViewById(R.id.tv_doc_info_title)).setText(_document._title);
		((TextView)findViewById(R.id.tv_doc_info_author)).setText(_document._author);
		((TextView)findViewById(R.id.tv_doc_info_date)).setText(_dateFormat.format(new Date(_document._publicationDate)));
		((TextView)findViewById(R.id.tv_doc_info_description)).setText(_document._summary);
		((TextView)findViewById(R.id.tv_doc_info_keywords)).setText(_document._keywords);
		((TextView)findViewById(R.id.tv_doc_info_legal)).setText(_document._legalInfos);
		((TextView)findViewById(R.id.tv_doc_info_version)).setText(_document._version);
		((TextView)findViewById(R.id.tv_doc_info_lastbuilddate)).setText(_dateFormat.format(new Date(_document._lastBuildDate)));
		
		MyLogs.d(Utils.formatString(getString(string.access_doc_info_title), _document._title));
		(findViewById(R.id.tv_doc_info_title)).setContentDescription(Utils.formatString(getString(string.access_doc_info_title), _document._title));
		(findViewById(R.id.tv_doc_info_author)).setContentDescription(Utils.formatString(getString(string.access_doc_info_auteur), _document._author));
		(findViewById(R.id.tv_doc_info_date)).setContentDescription(Utils.formatString(getString(string.access_doc_info_pubDate), _dateFormat.format(new Date(_document._publicationDate))));
		(findViewById(R.id.tv_doc_info_description)).setContentDescription(Utils.formatString(getString(string.access_doc_info_description), _document._summary));
		(findViewById(R.id.tv_doc_info_keywords)).setContentDescription(_document._keywords+"\n");
		(findViewById(R.id.tv_doc_info_legal)).setContentDescription(_document._legalInfos+"\n");
		(findViewById(R.id.tv_doc_info_version)).setContentDescription(_document._version+"\n");
		(findViewById(R.id.tv_doc_info_lastbuilddate)).setContentDescription(_dateFormat.format(new Date(_document._lastBuildDate)));
		

	}



	
}
