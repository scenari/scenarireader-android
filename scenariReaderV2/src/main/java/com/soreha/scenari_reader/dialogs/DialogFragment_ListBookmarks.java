package com.soreha.scenari_reader.dialogs;


import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.soreha.scenari_reader.R;
import com.soreha.scenari_reader.R.id;
import com.soreha.scenari_reader.lib.data.Bookmark.Bookmarks;
import com.soreha.scenari_reader.lib.data.Bookmark.BookmarksInfos;
import com.soreha.scenari_reader.lib.util.MyLogs;

public class DialogFragment_ListBookmarks extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

	private Context _context;
	public MySimpleCursorAdapter _listAdapter;
	private int _documentId = -1;
	private ListView _listView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.dialog_list_bookmarks);
		_documentId = getIntent().getIntExtra("documentId",-1);

		init();
	}

	public void init() {
		setResult(RESULT_CANCELED);
		MyLogs.write("Available docs List Channel Created", MyLogs.TITLE);

		_context = getApplicationContext();

		setTitle(R.string.bookmaks_dialog_title);

		_listView = (ListView) findViewById(R.id.list_available_docs);
		_listView.setEmptyView(findViewById(R.id.list_available_docs_empty));

		final String[] cols = new String[] { BookmarksInfos.PAGE_TITLE, BookmarksInfos.DOCUMENT_TITLE, BookmarksInfos.CHANNEL_TITLE };
		final int[] views = new int[] { id.tv_bookmark_page_title,id.tv_bookmark_document_title ,id.tv_bookmark_channel_title };
		_listAdapter = new MySimpleCursorAdapter(_context, R.layout.item_list_bookmarks, null, cols, views, 0);

		_listView.setAdapter(_listAdapter);
		_listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				Cursor cursor  = (Cursor) _listAdapter.getItem(arg2);
				Intent resultIntent = new Intent();
				resultIntent.putExtra("documentId", cursor.getString(cursor.getColumnIndex(Bookmarks.DOCUMENT_ID)) );
				resultIntent.putExtra("url", cursor.getString(cursor.getColumnIndex(Bookmarks.PAGE_URI)) );
				setResult(RESULT_OK, resultIntent);

				DialogFragment_ListBookmarks.this.finish();
			}
		});

		getLoaderManager().initLoader(0, null, this);
	}

	private class MySimpleCursorAdapter extends SimpleCursorAdapter {
		public MySimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
			super(context, layout, c, from, to, flags);
		}
		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			super.bindView(view, context, cursor);
		}
		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			return super.newView(context, cursor, parent);
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		if (_documentId != -1)
			return new CursorLoader(_context, BookmarksInfos.CONTENT_URI, null, BookmarksInfos.DOCUMENT_ID + " = " + _documentId, null, null);
		else
			return new CursorLoader(_context, BookmarksInfos.CONTENT_URI, null, null, null, BookmarksInfos.DOCUMENT_ID);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		_listAdapter.swapCursor(arg1);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		_listAdapter.swapCursor(null);
	}
}
