package com.soreha.scenari_reader.dialogs;

import android.app.LoaderManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.soreha.scenari_reader.R;
import com.soreha.scenari_reader.lib.comm.RequestManager;
import com.soreha.scenari_reader.lib.data.Document;
import com.soreha.scenari_reader.lib.data.Document.Documents;
import com.soreha.scenari_reader.lib.service.DownloadService;
import com.soreha.scenari_reader.lib.service.IRemoteBackgroundService;
import com.soreha.scenari_reader.lib.util.MyLogs;
import com.soreha.scenari_reader.lib.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;

public class DialogFragment_DownloadManager extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

	public MySimpleCursorAdapter _listAdapter;
	private ListView _listView;
	public IRemoteBackgroundService _serviceDocDownloader;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.dialog_download_manager);

		init();
	}


	@Override
	protected void onResume() {
		super.onResume();
		Intent serviceIntent = new Intent(getApplicationContext(), DownloadService.class);
		serviceIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		bindService(serviceIntent, remoteConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onPause() {
		unbindService(remoteConnection);
		super.onPause();
	}

	private ServiceConnection remoteConnection = new ServiceConnection() {
		public void onServiceDisconnected(ComponentName name) {
			MyLogs.write("DISCONNECT SERVICE", MyLogs.TITLE);
			_serviceDocDownloader = null;
		}

		public void onServiceConnected(ComponentName name, IBinder service) {
			_serviceDocDownloader = IRemoteBackgroundService.Stub.asInterface(service);
		}
	};


	public void init() {
		LoaderManager _loaderManager = getLoaderManager();
		
		setTitle(R.string.dl_manager_dialog_title);

		final String[] cols = new String[] { Documents.TITLE, Documents.THUMB_SMALL_LOCAL_URI };
		final int[] views = new int[] { R.id.document_title };
		_listAdapter = new MySimpleCursorAdapter(getApplicationContext(), R.layout.item_list_download_manager, null, cols, views, 0);

		_listView = (ListView) findViewById(R.id.list_available_docs);
		_listView.setEmptyView(findViewById(R.id.list_available_docs_empty));
		_listView.setAdapter(_listAdapter);
		_listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		_listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View itemView, int arg2, long arg3) {
				if (Utils.getCheckedItemCount(_listView)>0)
					((TextView)findViewById(R.id.tv_download_manager_selection)).setText(R.string.dl_manager_selection);
				else
					((TextView)findViewById(R.id.tv_download_manager_selection)).setText(R.string.dl_manager_all);
				_listAdapter.notifyDataSetChanged();
			}
		});
		
		
		( findViewById(R.id.button1)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				resumeDownloads();
			}
		});

		( findViewById(R.id.button2)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				suspendDownloads();
			}
		});

		( findViewById(R.id.button3)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cancelDownloads();
			}
		});

        _loaderManager.initLoader(-111111, null, DialogFragment_DownloadManager.this);
	}
	
	private class MySimpleCursorAdapter extends SimpleCursorAdapter {

		public HashMap<String, Bitmap> _imageCache = new HashMap<String, Bitmap>();
		public HashMap<String, String> _downloadedImage = new HashMap<String, String>();
		RequestManager requestManager = new RequestManager(getApplicationContext(),null);

		public MySimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
			super(context, layout, c, from, to, flags);
		}

		
		public Bitmap getDownloadedImage(String imageLocalUri) {
			Bitmap bm;
			if (_imageCache.containsKey(imageLocalUri)) {
				bm = _imageCache.get(imageLocalUri);
			} else {
				bm = BitmapFactory.decodeFile(imageLocalUri);
				_imageCache.put(imageLocalUri, bm);
			}
			
			return bm;
		}
		
		public Bitmap getImageForDocument(Cursor cursor){
			String documentSmallThumLocalUri = cursor.getString(cursor.getColumnIndex(Documents.THUMB_SMALL_LOCAL_URI));
			String documentSmallThumbUrl = cursor.getString(cursor.getColumnIndex(Documents.THUMB_SMALL_URL));
			
			Bitmap bm = null;
			if (!documentSmallThumLocalUri.equals("")) {
				bm = getDownloadedImage(documentSmallThumLocalUri);
				_downloadedImage.put(documentSmallThumbUrl, documentSmallThumLocalUri);
			} else {
				if (!documentSmallThumbUrl.equals("")) {
					if (_downloadedImage.containsKey(documentSmallThumbUrl)) {
						if (!_downloadedImage.get(documentSmallThumbUrl).equals("")) {
							bm = getDownloadedImage(_downloadedImage.get(documentSmallThumbUrl));
							Document document = new Document(getApplicationContext());
							document.initDataWithCursor(cursor);
								document._smallThumbLocalUri = _downloadedImage.get(documentSmallThumbUrl);
							document.updateThumb(getContentResolver());
						} else {
						}
					} else {
						_downloadedImage.put(documentSmallThumbUrl, "");
						requestManager.addThumbnailRequest(cursor.getInt(cursor.getColumnIndex(Documents.DOCUMENT_ID)),false);
					}
				}
			}
			return bm;
		}
		

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			final ImageView iv = (ImageView) view.findViewById(R.id.iv_document_smallthumb);
			Bitmap bm = getImageForDocument(cursor);
			if (bm != null){
				iv.setImageBitmap(bm);
			} else {
				iv.setImageResource(R.drawable.ic_default_thumb);
			}
			
			final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
			progressBar.setProgress(cursor.getInt(cursor.getColumnIndex(Documents.STATUS_PROGRESS)));
			
			final TextView tv = (TextView) view.findViewById(R.id.tv_status);
			tv.setTextColor(0xffffbb33);		
			
			((ProgressBar) view.findViewById(R.id.progressBar1)).setVisibility(View.INVISIBLE);
			switch (cursor.getInt(cursor.getColumnIndex(Documents.STATUS))) {
			case Document.STATUS_DOWNLOADING:
				tv.setText(R.string.doc_status_downloading);
				progressBar.setVisibility(View.VISIBLE);
				break;
			case Document.STATUS_WAITING_FOR_DOWNLOAD:
				tv.setText(R.string.doc_status_waiting);
				break;
			case Document.STATUS_DOWNLOAD_PAUSED:
				tv.setText(R.string.doc_status_paused);
				break;
			case Document.STATUS_DOWNLOAD_INTERRUPTED:
				tv.setText(R.string.doc_status_downloading);
				break;
			case Document.STATUS_EXTRACTING:
				tv.setText(R.string.doc_status_extracting);
				progressBar.setVisibility(View.VISIBLE);
				break;
			default:
				if (cursor.getInt(cursor.getColumnIndex(Documents.UPDATE_AVAILABLE)) == 1)
					tv.setText(R.string.doc_status_update_available);
				else
					tv.setText("");
				break;
			}
			
			final CheckBox ch = (CheckBox) view.findViewById(R.id.checkbox);
			ch.setChecked(_listView.isItemChecked(cursor.getPosition()));
			
			super.bindView(view, context, cursor);
		}
	}
	
	public void resumeDownloads() {
		ArrayList<String> selectedDocIds = getSelectedDocIds();
		if (selectedDocIds.size() > 0) {
			MyLogs.d("Resume selected items : " + selectedDocIds.size());
			try {
				_serviceDocDownloader.resumeDownloadForDocuments(selectedDocIds);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
	}

	public void cancelDownloads() {
		ArrayList<String> selectedDocIds = getSelectedDocIds();
		MyLogs.d("NbSelected Item : "+Utils.getCheckedItemCount(_listView));
		if (selectedDocIds.size() > 0) {
			MyLogs.d("Cancel selected items : " + selectedDocIds.size());
			try {
				_serviceDocDownloader.cancelDownloadForDocuments(selectedDocIds);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		} 
	}

	public void suspendDownloads() {
		ArrayList<String> selectedDocIds = getSelectedDocIds();
		if (selectedDocIds.size() > 0) {
			try {
				_serviceDocDownloader.pauseDownloadForDocuments(selectedDocIds);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			MyLogs.d("pause selected");
		} 
	}
	
	public ArrayList<String> getSelectedDocIds(){
		SparseBooleanArray array = _listView.getCheckedItemPositions();
		ArrayList<String> selectedDocIds = new ArrayList<String>();
		MyLogs.d("Taille sparse boolean : "+array.size());
		
		boolean forAll = false;
		if (Utils.getCheckedItemCount(_listView)==0)
			forAll = true;
		
		int size = _listAdapter.getCount();
		for (int i = 0; i < size ; i++){
			MyLogs.d(i+" - "+array.get(i));
			if (array.get(i) || forAll){
				Cursor cursor = (Cursor)_listAdapter.getItem(i);
				selectedDocIds.add(cursor.getInt(cursor.getColumnIndex(Documents.DOCUMENT_ID))+"");
			}
		}
		
		return selectedDocIds;
	}


	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		return new CursorLoader(getApplicationContext(), Documents.CONTENT_URI, null,
				Documents.STATUS + " = " + Document.STATUS_DOWNLOADING + " OR " +
						Documents.STATUS + " = " + Document.STATUS_WAITING_FOR_DOWNLOAD + " OR " +
						Documents.STATUS + " = " + Document.STATUS_DOWNLOAD_INTERRUPTED + " OR " +
						Documents.STATUS + " = " + Document.STATUS_DOWNLOAD_PAUSED + " OR " +
						Documents.STATUS + " = " + Document.STATUS_DOWNLOAD_ERROR + " OR " +
						Documents.STATUS + " = " + Document.STATUS_EXTRACTING + " OR " +
						Documents.STATUS + " = " + Document.STATUS_EXTRACT_ERROR + " OR " +
						Documents.UPDATE_AVAILABLE + " = 1"
				, null, Documents.RSS_POS
						+ " ASC");
	}


	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		_listAdapter.swapCursor(arg1);
	}


	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		_listAdapter.swapCursor(null);
	}
}
