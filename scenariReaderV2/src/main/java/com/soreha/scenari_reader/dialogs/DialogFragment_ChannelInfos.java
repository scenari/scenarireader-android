package com.soreha.scenari_reader.dialogs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.soreha.scenari_reader.R;
import com.soreha.scenari_reader.R.string;
import com.soreha.scenari_reader.lib.data.Channel;
import com.soreha.scenari_reader.lib.util.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DialogFragment_ChannelInfos extends AppCompatActivity {

	private final DateFormat _dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
	private Channel _channel;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.dialog_channel_infos);

		_channel = getIntent().getParcelableExtra("channel");

		initViews();
	}

	public void initViews() {

		setTitle(string.channel_info_dialog_title);

		((TextView) findViewById(R.id.tv_doc_info_title)).setText(_channel._title);
		((TextView) findViewById(R.id.tv_doc_info_date)).setText(_dateFormat.format(new Date(_channel._publicationDate)));
		((TextView) findViewById(R.id.tv_doc_info_description)).setText(_channel._summary);
		((TextView) findViewById(R.id.tv_doc_info_legal)).setText(_channel._legalInfos);
		((TextView) findViewById(R.id.tv_doc_info_nbdocs)).setText(_channel._nbDoc + "");
		((TextView) findViewById(R.id.tv_doc_info_lastbuilddate)).setText(_dateFormat.format(new Date(_channel._lastBuildDate)));
		
		(findViewById(R.id.tv_doc_info_title)).setContentDescription(Utils.formatString(getString(string.access_channel_info_title),_channel._title));
		(findViewById(R.id.tv_doc_info_date)).setContentDescription(Utils.formatString(getString(string.access_channel_info_pubDate),_dateFormat.format(new Date(_channel._publicationDate))));
		(findViewById(R.id.tv_doc_info_description)).setContentDescription(Utils.formatString(getString(string.access_channel_info_description),_channel._summary));
		(findViewById(R.id.tv_doc_info_legal)).setContentDescription(_channel._legalInfos+"\n");
		(findViewById(R.id.tv_doc_info_nbdocs)).setContentDescription(_channel._nbDoc + "\n");
		(findViewById(R.id.tv_doc_info_lastbuilddate)).setContentDescription(_dateFormat.format(new Date(_channel._lastBuildDate)));
	}
}
