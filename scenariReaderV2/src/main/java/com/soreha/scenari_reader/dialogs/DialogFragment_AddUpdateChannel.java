package com.soreha.scenari_reader.dialogs;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.URLUtil;
import android.widget.EditText;

import com.soreha.scenari_reader.R;
import com.soreha.scenari_reader.lib.data.Channel;
import com.soreha.scenari_reader.lib.util.MyLogs;
import com.soreha.scenari_reader.lib.util.Utils;
import com.soreha.scenari_reader.zxing.IntentIntegrator;
import com.soreha.scenari_reader.zxing.IntentResult;

public class DialogFragment_AddUpdateChannel extends AppCompatActivity {

	private Channel _channel;



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_add_edit_channel);
		_channel = getIntent().getParcelableExtra("channel");
		init();
	}



	public void init() {

		MyLogs.write("Available docs List Channel Created", MyLogs.TITLE);

		setTitle(R.string.add_channel_dialog_title);
		//setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_action_channel);

		if (_channel != null) {
			((EditText) findViewById(R.id.editText1)).setText(_channel._rssUrl);
			if (_channel._status == Channel.STATUS_VALIDATED){
				( findViewById(R.id.editText1)).setEnabled(false);
				( findViewById(R.id.button_scan)).setEnabled(false);
			}
			if (!_channel._userTitle.equals(""))
				((EditText) findViewById(R.id.editText2)).setText(_channel._userTitle);
		}

		findViewById(R.id.button1).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				submit();
			}
		});

		findViewById(R.id.button2).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
				finish();
			}
		});
		
		findViewById(R.id.button_scan).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				scanQRCode(null);
			}
		});

	}

	public void scanQRCode (View v){
		IntentIntegrator integrator = new IntentIntegrator(this);
		integrator.initiateScan();
	}
	
	public void onActivityResult(int requestCode, int resultCode, final Intent intent) {
		MyLogs.t("onActivityResult");
		IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		  if (scanResult != null) {
			  ((EditText)findViewById(R.id.editText1)).setText(scanResult.getContents());
		  }
	}
	
	


	public void submit() {
		if (_channel == null) {
			_channel = new Channel(getApplicationContext());
			_channel._rssUrl = ((EditText) findViewById(R.id.editText1)).getText().toString();
			_channel._userTitle = ((EditText) findViewById(R.id.editText2)).getText().toString();

			if (URLUtil.isValidUrl(_channel._rssUrl)) {
				MyLogs.write("Valid URL", MyLogs.DEBUG);
				if (Channel.isChannelInDBForURL(getContentResolver(), _channel._rssUrl) == -1) {
					_channel.insertChannel(getContentResolver());
				}

				Intent intent = new Intent();
				intent.putExtra("channel", _channel);
				setResult(RESULT_OK, intent);
				finish();
			} else {
				MyLogs.write("Invalid URL", MyLogs.DEBUG);
				Utils.showStandardAlertDialog(this,getString(R.string.add_channel_error_dialog_title),
						getString(R.string.add_channel_error_dialog_message));
			}

		} else {
			_channel._userTitle = ((EditText) findViewById(R.id.editText2)).getText().toString();
			if (URLUtil.isValidUrl(_channel._rssUrl)) {
				MyLogs.write("Old channel, valid URL", MyLogs.DEBUG);
				showProgressBar();
				Intent intent = new Intent();
				intent.putExtra("channel", _channel);
				setResult(RESULT_OK, intent);
				finish();
			} else {
				MyLogs.write("Invalid URL", MyLogs.DEBUG);
				Utils.showStandardAlertDialog(this, getString(R.string.add_channel_error_dialog_title),
						getString(R.string.add_channel_error_dialog_message));
			}
		}
	}

	public void showProgressBar() {
		setProgressBarIndeterminateVisibility(true);
	}

	public void hidProgressBar() {
		setProgressBarIndeterminateVisibility(false);
	}

}
